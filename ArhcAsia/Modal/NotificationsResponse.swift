//
//  NotificationsResponse.swift
//  ArhcAsia
//
//  Created by Renendra Saxena on 29/09/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation
struct NotificationsResponse {
    
    var id: String?
    var title: String?
    var message: String?
    var user_id: Int?
    var notification_type: String?
    var redirect_id: String?
    var created_at: String?
    var updated_at: String?

    
    
    init(responseDict: NSDictionary) {
        self.id          = responseDict.value(forKey: "id") as! String
        self.title       = responseDict.value(forKey: "title") as? String
        self.message = responseDict.value(forKey: "message") as? String
        self.user_id          = responseDict.value(forKey: "user_id") as! Int
        self.notification_type = responseDict.value(forKey: "notification_type") as? String
        self.redirect_id = responseDict.value(forKey: "redirect_id") as? String
        self.created_at = responseDict.value(forKey: "created_at") as? String
        self.updated_at = responseDict.value(forKey: "updated_at") as? String
        
    }
}


struct NotificationsData {
}
