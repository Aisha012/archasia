//
//  PageModal.swift
//  ToolApp
//
//  Created by Phaninder on 04/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

struct PageModal {
    
    var pageId: String?
    var pageName: String?
    var pageDescription: String?
    var eventId: String?
    
    init(responseDict: NSDictionary, eventId: String) {
        self.eventId = eventId
        self.pageId          = "\(responseDict.value(forKey: "id") as! Int)"
        self.pageName       = responseDict.value(forKey: "name") as? String
        self.pageDescription = responseDict.value(forKey: "description") as? String
        
    }
    
    
}
