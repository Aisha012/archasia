//
//  ChallengesModal.swift
//  ArhcAsia
//
//  Created by Namespace  on 06/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation
struct ChallengesModal {
    var id: Int?
    var title: String?
    var description: String?
    var points: Int?
    
    var eventId: String?
    
    init(responseDict: NSDictionary, eventId: String) {
        self.eventId = eventId
        self.id          = responseDict.value(forKey: "id") as! Int
        self.title       = responseDict.value(forKey: "title") as? String
        self.description = responseDict.value(forKey: "description") as? String
        self.points          = responseDict.value(forKey: "points") as! Int
        
    }
}
