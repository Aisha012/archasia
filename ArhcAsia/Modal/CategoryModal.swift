//
//  CategoryModal.swift
//  ToolApp
//
//  Created by Zaman Meraj on 06/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import Foundation


struct CategoryModal {
    
    var category_id: String?
    var color: String?
    var created_at: String?
    var updated_at: String?
    var user_id: String?
    var name: String?
    var agendaId: String?
    init(responseDict: NSDictionary, agendaId: String) {
        self.category_id = "\(responseDict.value(forKey: "id") as! Int)"
        self.color       = responseDict.value(forKey: "color") as? String
        self.created_at  = responseDict.value(forKey: "created_at") as? String
        self.updated_at  = responseDict.value(forKey: "updated_at") as? String
        self.user_id     = responseDict.value(forKey: "user_id") as? String
        self.name        = responseDict.value(forKey: "name") as? String
        self.agendaId    = agendaId
    }
    
}

struct RoomModal {
    
    var id: String?
    var user_id: String?
    var created_at: String?
    var updated_at: String?
    var name: String?
    var agendaId: String?
    init(responseDict: NSDictionary, agendaId: String) {
        
        self.id         = "\(responseDict.value(forKey: "id") as! Int)"
        self.user_id    =       responseDict.value(forKey: "user_id") as? String
        self.created_at =       responseDict.value(forKey: "created_id") as? String
        self.updated_at =       responseDict.value(forKey: "created_at") as? String
        self.name       =       responseDict.value(forKey: "name") as? String
        self.agendaId   =       agendaId
    }
    
}


struct SpeakersModal {
    
    var speaker_id: String?
    var name: String?
    var title: String?
    var subTitle: String?
    var description: String?
    var avatar:String?
    var agendaId: String?
    var facebookLink: String!
    var linkedInLink: String!
    var googleLink: String!
    var twitterLink: String!
    var votes = 0
    var hasVoted = false
    
    init(responseDict: NSDictionary, agendaId: String) {
        
        self.speaker_id = "\(responseDict.value(forKey: "id") as! Int)"
        self.name =  responseDict.value(forKey: "name") as? String
        self.title = responseDict.value(forKey: "title") as? String
        self.subTitle = responseDict.value(forKey: "sub_title") as? String
        self.description = responseDict.value(forKey: "description") as? String
        self.avatar = responseDict.value(forKey: "avatar") as? String
        self.facebookLink = responseDict.value(forKey: "facebook_link") as? String ?? ""
        self.linkedInLink = responseDict.value(forKey: "linkedin_link") as? String ?? ""
        self.googleLink = responseDict.value(forKey: "google_link") as? String ?? ""
        self.twitterLink = responseDict.value(forKey: "twitter_link") as? String ?? ""
        self.agendaId = agendaId
        self.votes = responseDict.value(forKey: "votes") as? Int ?? 0
        self.hasVoted = responseDict.value(forKey: "voted") as? Bool ?? false
    }
    
}



