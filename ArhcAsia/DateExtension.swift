//
//  DateExtension.swift
//  ToolApp
//
//  Created by Zaman Meraj on 06/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import Foundation
class DateExtension {
    
    static func changeDateFormat(date: String, getDateFormat: String, desiredDateFormat: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = getDateFormat
        
        let getDate = dateFormatter.date(from: date)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = desiredDateFormat
        let desiredDate = dateFormatter2.string(from: getDate!)
        return desiredDate
        
    }
    
    static func changeDateToString(date: Date, dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let stringDate = dateFormatter.string(from: date)
        return stringDate
    }
    
    static func changeStringToDate(dateString: String, getFormat: String, desiredDateFormat: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = getFormat
        let date = dateFormatter.date(from: dateString)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = desiredDateFormat
        let requireDate = dateFormatter2.string(from: date!)
        let finalDate = dateFormatter2.date(from: requireDate)
        return finalDate!
    }
    
    static func changeTimeStampToDate(value: String) -> Date {
        
        let timeStamp = NumberFormatter().number(from: value)
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp as! Int))
        
        return date
    }
    
}
