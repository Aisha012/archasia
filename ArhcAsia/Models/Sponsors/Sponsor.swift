//
//  Sponsor.swift
//  ToolApp
//
//  Created by Phaninder on 19/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class Sponsor {
    
    let id: Int!
    let name: String!
    let logo: String!
    let website: String!
    let category: String!
    let info: String!
    let htmlInfo: NSAttributedString!
    
    init(responseDict: NSDictionary) {
        self.id = responseDict.value(forKey: "id") as? Int ?? 0
        self.name = responseDict.value(forKey: "name") as? String ?? ""
        self.logo = responseDict.value(forKey: "logo") as? String ?? ""
        self.website = responseDict.value(forKey: "website") as? String ?? ""
        self.category = responseDict.value(forKey: "category") as? String ?? ""
        self.info = responseDict.value(forKey: "description") as? String ?? ""
        self.htmlInfo = self.info.convertHtml(fontFamilyName: "SanFranciscoText", fontSize: 15)
    }
}
