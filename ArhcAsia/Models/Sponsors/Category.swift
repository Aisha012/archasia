//
//  Category.swift
//  ToolApp
//
//  Created by Phaninder on 14/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class Category {
    
    let id: Int!
    let userId: String!
    let name: String!
    
    init(responseDict: NSDictionary) {
        self.id = responseDict.value(forKey: "id") as? Int ?? 0
        self.name = responseDict.value(forKey: "name") as? String ?? ""
        self.userId = String(responseDict.value(forKey: "category") as? Int ?? 0)
    }
}

