//
//  BreakOutSession.swift
//  ToolApp
//
//  Created by Phaninder on 26/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class BreakOutSession {
    
    let name: String!
    let desc: String!
    let startTime: String!
    let endTime: String!
    
    
    init(responseDict: NSDictionary) {
        self.name = responseDict.value(forKey: "name") as? String ?? ""
        self.desc = responseDict.value(forKey: "description") as? String ?? ""
        self.startTime = responseDict.value(forKey: "start_time") as? String ?? ""
        self.endTime = responseDict.value(forKey: "end_time") as? String ?? ""
    }

    
}
