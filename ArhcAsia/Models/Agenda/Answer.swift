//
//  Answer.swift
//  ToolApp
//
//  Created by Phaninder on 18/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class Answer {
    
    let id: Int!
    let title: String!
    let answer: String!
    let createdAt: String!
    let formattedDate: String!
    let role: String!
    let avatar: String!
    let firstName: String!
    let lastName: String!
    let userId: Int!
    
    
    init(responseDict: NSDictionary) {
        self.id = responseDict.value(forKey: "id") as? Int ?? 0
        self.title = responseDict.value(forKey: "question_title") as? String ?? ""
        self.answer = responseDict.value(forKey: "answer") as? String ?? ""
        self.createdAt = responseDict.value(forKey: "created_at") as? String ?? ""
        
        let createdDate: Date!
        let dateFormatter = DateFormatter()
        if let date = self.createdAt, !date.isEmpty {
            let index = date.index(date.startIndex, offsetBy: 16)
            let myDateString = String(date[..<index])
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
            createdDate = dateFormatter.date(from: myDateString)!
            
        } else {
            createdDate = Date()
        }
        
        dateFormatter.dateFormat = "E, d MMM yyyy"
        formattedDate = dateFormatter.string(from: createdDate)
        
        if let userDict = responseDict.value(forKey: "user") as? NSDictionary {
            self.firstName = userDict.value(forKey: "first_name") as? String ?? ""
            self.lastName = userDict.value(forKey: "last_name") as? String ?? ""
            self.avatar = userDict.value(forKey: "avatar") as? String ?? ""
            self.role = userDict.value(forKey: "role") as? String ?? "Normal"
            self.userId = userDict.value(forKey: "id") as? Int ?? 0
        } else {
            self.firstName = ""
            self.lastName = ""
            self.avatar = ""
            self.role = "Normal"
            self.userId = 0
        }
        
    }
    
}
