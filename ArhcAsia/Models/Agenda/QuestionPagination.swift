//
//  QuestionPagination.swift
//  ToolApp
//
//  Created by Phaninder on 05/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class QuestionPagination {
    
    var questions: [Question] = []
    private let limit = 5
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool = false
    
    init() {
        questions = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    init(array: [NSDictionary]) {
        for dict in array {
            let question = Question(responseDict: dict)
            self.questions.append(question)
        }
        
        hasMoreToLoad = self.questions.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: QuestionPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.questions = [];
            self.questions = newPaginationObject.questions
        case .old:
            self.questions += newPaginationObject.questions
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
