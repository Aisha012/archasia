//
//  GalleryPagination.swift
//  ToolApp
//
//  Created by Phaninder on 20/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class GalleryPagination {
    
    var photos: [GalleryFile] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool = false
    
    init() {
        photos = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    init(array: [NSDictionary]) {
        for dict in array {
            let photo = GalleryFile(responseDict: dict)
            self.photos.append(photo)
        }
        
        hasMoreToLoad = self.photos.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: GalleryPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.photos = [];
            self.photos = newPaginationObject.photos
        case .old:
            self.photos += newPaginationObject.photos
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
