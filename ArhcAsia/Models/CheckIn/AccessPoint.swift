//
//  AccessPoint.swift
//  ToolApp
//
//  Created by Phaninder on 20/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class AccessPoint {
    
    let id: Int!
    let name: String!
    let exitTrack: Bool!
    let qrCode: String!
    let createdAt: String!
    
    init(responseDict: NSDictionary) {
        self.id = responseDict.value(forKey: "id") as? Int ?? 0
        self.name = responseDict.value(forKey: "name") as? String ?? ""
        self.exitTrack = responseDict.value(forKey: "exit_track") as? Bool ?? false
        self.qrCode = responseDict.value(forKey: "qr_code_string") as? String ?? ""
        self.createdAt = responseDict.value(forKey: "created_at") as? String ?? ""
    }

}
