//
//  PollOption.swift
//  ToolApp
//
//  Created by Phaninder on 17/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class PollOption {
    
    let id: String!
    let option: String!
    
    init(responseDict: NSDictionary) {
        
        self.id = String(responseDict.value(forKey: "id") as? Int ?? 0)
        self.option = responseDict.value(forKey: "option") as? String ?? ""
        
    }
}
