//
//  Poll.swift
//  ToolApp
//
//  Created by Phaninder on 17/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class Poll {
    
    let id: String!
    let question: PollQuestion!
    var options: [PollOption]!
    var isSubmitted: Bool = false
    var selectedOptionId: Int?
    
    init(responseDict: NSDictionary) {
        self.id = String(responseDict.value(forKey: "id") as? Int ?? 0)
        let questionDict = responseDict.value(forKey: "question") as! NSDictionary
        self.question = PollQuestion(responseDict: questionDict)
        self.options = [PollOption]()
        if let optionsArray = responseDict.value(forKey: "options") as? [NSDictionary] {
            for optionDict in optionsArray {
                let option = PollOption(responseDict: optionDict)
                self.options.append(option)
            }
        }
        self.isSubmitted = responseDict.value(forKey: "is_submitted") as? Bool ?? false
        self.selectedOptionId = responseDict.value(forKey: "selected_option_id") as? Int
    }

}
