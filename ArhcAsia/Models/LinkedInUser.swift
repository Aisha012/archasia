//
//  LinkedInUser.swift
//  ToolApp
//
//  Created by Phaninder on 07/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class LinkedInUser {
    
    let id: String!
    let firstName: String!
    let lastName: String!
    let email: String!
    
    init(responseDict: [String: AnyObject]) {
        self.id = responseDict["id"] as? String ?? ""
        self.firstName = responseDict["firstName"] as? String ?? ""
        self.lastName = responseDict["lastName"] as? String ?? ""
        self.email = responseDict["emailAddress"] as? String ?? ""
    }
    
    
}
