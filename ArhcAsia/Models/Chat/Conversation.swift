//
//  Conversation.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class Conversation {
    
    let id: String!
    let clientId: Int!
    let firstName: String!
    let lastName: String!
    let email: String!
    let avatar: String!
    let phone: String!
    let authenticationToken: String!
    let role: String!
    let qrCode: String!
    let lastMessage: String!
    let updateAt: String!
    let seen: Bool!
    let createdDate: Date!
    let elapsedTime: String!
    let position: String!
    let organization: String!
    
    init(responseDict: NSDictionary) {
        self.id = responseDict.value(forKey: "id") as? String ?? ""
        if let identityDict = responseDict.value(forKey: "identity") as? NSDictionary {
            self.clientId = identityDict.value(forKey: "id") as? Int ?? 0
            self.firstName = identityDict.value(forKey: "first_name") as? String ?? ""
            self.lastName = identityDict.value(forKey: "last_name") as? String ?? ""
            self.email = identityDict.value(forKey: "email") as? String ?? ""
            self.avatar = identityDict.value(forKey: "avatar") as? String ?? ""
            self.role = identityDict.value(forKey: "role") as? String ?? "Normal"
            self.qrCode = identityDict.value(forKey: "qr_code_string") as? String ?? ""
            self.phone = identityDict.value(forKey: "mobile") as? String ?? ""
            self.authenticationToken = identityDict.value(forKey: "authentication_token") as? String ?? ""
            self.position = identityDict.value(forKey: "position") as? String ?? ""
            self.organization = identityDict.value(forKey: "organization") as? String ?? ""

        } else {
            self.clientId = 0
            self.firstName = ""
            self.lastName = ""
            self.email = ""
            self.qrCode = ""
            self.avatar = ""
            self.phone = ""
            self.role = ""
            self.authenticationToken = ""
            self.position = ""
            self.organization = ""
        }
        self.lastMessage = responseDict.value(forKey: "last_message") as? String ?? ""
        self.updateAt = responseDict.value(forKey: "updated_at") as? String ?? ""
        if let date = self.updateAt, !date.isEmpty {
            let index = date.index(date.startIndex, offsetBy: 16)
            let myDateString = String(date[..<index])
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
            createdDate = dateFormatter.date(from: myDateString)!
            elapsedTime = createdDate.getElapsedInterval()
        } else {
            createdDate = Date()
            elapsedTime = createdDate.getElapsedInterval()
        }

    
        self.seen = responseDict.value(forKey: "seen") as? Bool ?? false
    }

    
}
