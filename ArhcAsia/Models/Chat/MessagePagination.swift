//
//  MessagePagination.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class MessagePagination {
    
    var messages: [MessageInfo] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool = false
    
    init() {
        messages = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    init(array: [NSDictionary]) {
        for dict in array {
            let message = MessageInfo(responseDict: dict)
            self.messages.append(message)
        }
        
        self.messages.reverse()
        
        hasMoreToLoad = self.messages.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: MessagePagination) {
        switch self.paginationType {
        case .new, .reload:
            self.messages = [];
            self.messages = newPaginationObject.messages
        case .old:
            self.messages.insert(contentsOf: newPaginationObject.messages, at: 0)
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
