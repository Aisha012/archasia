//
//  MessageInfo.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation
import MessageKit

struct MessageInfo: MessageType {
    
    var messageId: String
    var sender: Sender
    var sentDate: Date
    var data: MessageData
    var image: String
    
    init(responseDict: NSDictionary) {
        self.messageId = responseDict.value(forKey: "id") as? String ?? ""
        let message = responseDict.value(forKey: "body") as? String ?? ""
        self.data = MessageData.text(message)
        if let userDict = responseDict.value(forKey: "user") as? NSDictionary {
            
            //Create Sender
            let userId = userDict.value(forKey: "id") as? Int ?? 0
            let userName = userDict.value(forKey: "first_name") as? String ?? "John"
            self.sender = Sender(id: "\(userId)", displayName: userName)
            
            //Create Sent date
            let stringDate = responseDict.value(forKey: "created_at") as? String ?? "2018-02-08T13:16:03.000Z"
            let index = stringDate.index(stringDate.startIndex, offsetBy: 16)
            let myDateString = String(stringDate[..<index])
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
            self.sentDate = dateFormatter.date(from: myDateString)!
            
            //Image
            self.image = userDict.value(forKey: "avatar") as? String ?? ""

        } else {
            self.sender = Sender(id: "0", displayName: "John")
            self.sentDate = Date()
            self.image = ""
        }
    }
    
}
