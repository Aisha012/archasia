//
//  ChatUser.swift
//  ToolApp
//
//  Created by Phaninder on 27/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class ChatUser {
    
    let id: Int!
    let firstName: String!
    let lastName: String!
    let email: String!
    let role: String!
    let phone: String!
    let avatar: String!
    let qrCode: String!
    let authenticationToken: String!
    let company: String!
    let position: String!

    init(conversation: Conversation) {
        self.id = conversation.clientId
        self.firstName = conversation.firstName
        self.lastName = conversation.lastName
        self.email = conversation.email
        self.avatar = conversation.avatar
        self.role = conversation.role
        self.authenticationToken = conversation.authenticationToken
        self.qrCode = conversation.qrCode
        self.phone = conversation.phone
        self.company = conversation.organization
        self.position = conversation.position
    }
    
}
