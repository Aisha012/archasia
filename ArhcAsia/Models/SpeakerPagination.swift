//
//  SpeakerPagination.swift
//  ToolApp
//
//  Created by Phaninder on 19/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class SpeakerPagination {
    
    var speakers: [SpeakersModal] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool = false
    
    init() {
        speakers = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    init(array: [NSDictionary]) {
        for dict in array {
            let speaker = SpeakersModal(responseDict: dict, agendaId: "0")
            self.speakers.append(speaker)
        }
        
        hasMoreToLoad = self.speakers.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: SpeakerPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.speakers = [];
            self.speakers = newPaginationObject.speakers
        case .old:
            self.speakers += newPaginationObject.speakers
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
