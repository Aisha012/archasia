//
//  PhotoGallaryVc.swift
//  ToolApp
//
//  Created by devlopment on 05/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import INSPhotoGallery
import Photos
import Alamofire
import OpalImagePicker

class PhotoGallaryVc: BaseClassVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var plusIcon: UIImageView!
    
    fileprivate let imageSession = URLSession(configuration: .default)
    var useCustomOverlay = false
    let picker = UIImagePickerController()
    var selectedMediaObject     =    Data()
    var fileData                =    Data()
    var eventId           :   String?
    var eventData: EventData!
    var pageIndex = 1
    var imageArray :[Data]      =    []
    var dirPath: String?
    var photosPagination = GalleryPagination()
    var showMenuButton = true
    var isFirstTime = true
    var isFetchingData = false
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id
        addPhotoButton.isHidden = eventData.allow_upload! != "1"
        plusIcon.isHidden = eventData.allow_upload! != "1"
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }

        self.titleLabel.text = "Gallery".localized
        fetchGallery()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate   = self
        self.collectionView.isScrollEnabled = true
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 4.0
            layout.minimumInteritemSpacing = 4.0
            layout.scrollDirection = .vertical
        }

        self.collectionView.reloadData()
    }
    
    //photosPagination.photos.count
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }

    func showViewsAccordingly() {
        if photosPagination.photos.count == 0 {
            collectionView.isHidden = true
            emptyPlaceholderLabel.isHidden = false
        } else {
            collectionView.reloadData()
            emptyPlaceholderLabel.isHidden = true
        }
    }

    @objc func refreshData(_ sender: Any) {
        photosPagination.paginationType = .reload
        fetchGallery()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    func fetchGallery() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            if photosPagination.paginationType != .old {
                self.photosPagination.currentPageNumber = 1
                if isFirstTime == true {
                    self.showHUD(forView: self.view, excludeViews: [])
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            isFetchingData = true
            
            _ = CallApi.getData(url: "http://yourevent2go.com/api/events/\(self.eventId!)/gallery?user_token=\(auth_Token)&api_token=\(apiToken)&page=\(photosPagination.currentPageNumber))", parameter: nil, type: .get) { (netResponse) -> (Void) in
                self.isFetchingData = false
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        let newPagination = GalleryPagination(array: JSON)
                        self.photosPagination.appendDataFromObject(newPagination)
                        if self.photosPagination.photos.count == 1 {
                            DispatchQueue.main.async {
                           self.presentGalleryPreview(index: 0, view: self.view)
                            return
                        }
                        }
                        DispatchQueue.main.async {
                            if self.photosPagination.paginationType != .old && self.isFirstTime == true {
                                self.hideHUD(forView: self.view)
                                self.isFirstTime = false
                            } else {
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                            self.showViewsAccordingly()
                        }
                    } else {
                        self.hideHUD(forView: self.view)
                    }
                }else {
                    self.hideHUD(forView: self.view)
                }
            }
        }
    }
    
    func uploadImages(_ images: [UIImage]) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String

        if let auth_Token = auth_Token {
            
            self.showHUD(forView: self.view, excludeViews: [])
            let params = ["user_token": auth_Token,
                          "api_token": apiToken,
                          "caption": [""]] as [String : Any]
            
            let url = "http://yourevent2go.com/api/events/\(eventId!)/galleries?user_token=\(auth_Token)&api_token=\(apiToken)"
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key, value) in params {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }

                // import image to request
                for (index, image) in images.enumerated() {
                    if let imageData = UIImageJPEGRepresentation(image, 0.5) {
                        multipartFormData.append(imageData, withName: "picture[\(index)]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    }
                }
            }, to: url,
               
               encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        self.photosPagination.paginationType = .reload
                        DispatchQueue.main.async {
                            self.hideHUD(forView: self.view)
                            self.isFirstTime = true
                            self.fetchGallery()
                        }
                    }
                case .failure(let error):
                    //Show alert
                    print(error)
                }
                
            })
        }
    }
    
    func generateBoundaryString() -> String{
        return "Boundary-\(UUID().uuidString)"
    }
    
    @IBAction func addMoreBtn(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image".localized, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { _ in
            self.camera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery".localized, style: .default, handler: { _ in
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
    
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension PhotoGallaryVc: UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryCollectionViewCell.cellIdentifier(), for: indexPath) as! GalleryCollectionViewCell
        
        cell.galleryImageView.sd_setImage(with: URL(string: photosPagination.photos[indexPath.row].banner), placeholderImage: UIImage(named: "gallery_placeholder"), options: .retryFailed, completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosPagination.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == photosPagination.photos.count - 1 && self.photosPagination.hasMoreToLoad && !isFetchingData {
            self.photosPagination.paginationType = .old
            fetchGallery()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! GalleryCollectionViewCell
        self.presentGalleryPreview(index: indexPath.row, view: cell)
    }
    
    func presentGalleryPreview(index:Int, view:UIView){
        let currentPhoto = photosPagination.photos[index].photo
        var photosArray = [INSPhotoViewable]()
        
        for photo in photosPagination.photos {
            photosArray.append(photo.photo)
        }
        let galleryPreview = INSPhotosViewController(photos: photosArray, initialPhoto: currentPhoto, referenceView: view)
        galleryPreview.hideShareButton(hide: eventData.allow_share! != "1")
        //        galleryPreview.overlayView.setHidden(true, animated: true)
        galleryPreview.referenceViewForPhotoWhenDismissingHandler = { [weak self] photo in
            if let index = self?.photosPagination.photos.index(where: {$0.photo === photo}) {
                let indexPath = IndexPath(item: index, section: 0)
                return self?.collectionView.cellForItem(at: indexPath) as? GalleryCollectionViewCell
            }
            return nil
        }
        present(galleryPreview, animated: true, completion: nil)
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionFooter, photosPagination.hasMoreToLoad == true {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterView", for: indexPath) as! GalleryFooterCollectionReusableView
            footerView.activityIndicator.startAnimating()
            return footerView
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if photosPagination.hasMoreToLoad == true {
            return CGSize.init(width: collectionView.frame.size.width, height: 50.0)
        } else {
            return CGSize.init(width: 0, height: 0)
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize.init(width: (self.collectionView.bounds.width/2) - 15, height: 145)
//    }
//
//
}

extension PhotoGallaryVc : UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.collectionView.bounds.width / 2) - 15 , height: self.view.frame.size.height * 0.2)
        
    }
}

extension PhotoGallaryVc: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary() {
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = 10

        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        
        let configuration = OpalImagePickerConfiguration()
        configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select more than 10 images", comment: "")
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        imagePicker.navigationBar.tintColor = UIColor.black
        imagePicker.selectionImageTintColor = UIColor.black
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        imagePicker.configuration = configuration
        imagePicker.imagePickerDelegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        uploadImages([image])
        dismiss(animated:true, completion: nil)
    }
    
}

extension PhotoGallaryVc: OpalImagePickerControllerDelegate {

    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        dismiss(animated: true , completion: nil)
        uploadImages(images)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        print("cancel")
    }
    
}




