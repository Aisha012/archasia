//
//  NotificationVC.swift
//  ArhcAsia
//
//  Created by Renendra Saxena on 29/09/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

class NotificationVC: BaseClassVC ,UITableViewDelegate , UITableViewDataSource {
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var bckBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var pages: [PageData]?
    @IBOutlet weak var noRecordLbl: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchNotificationsList()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        noRecordLbl.isHidden = true
        customNavBar.backgroundColor =  navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
//    // MARK:- Navigation_Bar_Color_Mthd
//    func navigationBarColor() -> UIColor {
//        if let color_Value  =  UserDefaults.standard.value(forKey: "color_Value") as? String {
//            return UIColor.colorWithHexString(hex: color_Value)
//        }else{
//            return UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
//        }
//    }
    @IBAction func navigateBack(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //    MARK: tableView Delegate and datasource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        noRecordLbl.isHidden = notificationList.count == 0 ? false : true
        return notificationList.count == 0 ? 0 : notificationList.count
    }
    
    var cell : notificationTVCell!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! notificationTVCell
        cell.notificationTitleLbl.text = notificationList[indexPath.row].title ?? ""
        cell.notificationLbl.text = notificationList[indexPath.row].message ?? ""
        cell.timeLbl.text = notificationList[indexPath.row].created_at ?? ""
        return cell
    }
    
    
    var notificationList = [NotificationsResponse]()
    
    func fetchNotificationsList() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        let eventID = UserDefaults.standard.value(forKey: "Event_Id") as? String
        pages = MyDatabase.matchProgramData(tableName: "PageData", tableId: eventID!, columnName: "event_id") as? [PageData]
        
                    _ = showHUD(forView: self.view, excludeViews: [])
                _ = CallApi.getData(url: "http://yourevent2go.com/api/events/\(eventID!)/notifications?user_token=\(auth_Token)&api_token=\(apiToken)&page=1", parameter: nil, type: .get) {[weak self] (netResponse) -> (Void) in
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                
                if let JSON = netResponse.responseDict as? [NSDictionary] {
                    
                    for jsonValue in JSON {
                        let completeModal = NotificationsResponse(responseDict: jsonValue)
                        self?.notificationList.append(completeModal)

                    }
                    if (self?.notificationList.count)! <= 0{
                        self?.view.makeToast("There is no notification listing".localized)
                    }
                    self?.hideHUD(forView: self?.view)

//                    self?.notifiaction = NotificationsResponse.init(responseDict: JSON)
                    self?.tableView.delegate = self
                    self?.tableView.dataSource = self
                    self?.tableView.reloadData()
                }else{
                    self?.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                }
            }else{
                self?.noRecordLbl.isHidden = self?.notificationList.count == 0 ? false : true
                self?.hideHUD(forView: self?.view)

                }

        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
class notificationTVCell : UITableViewCell{
    @IBOutlet var timeLbl : UILabel!
    @IBOutlet var notificationTitleLbl : UILabel!
    @IBOutlet var notificationLbl : UILabel!
    
    
}
