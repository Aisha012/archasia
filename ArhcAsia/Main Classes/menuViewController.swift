//
//  menuViewController.swift
//  memuDemo
//
//  Created by Parth Changela on 09/10/16.
//  Copyright © 2016 Parth Changela. All rights reserved.
//

import UIKit
import SDWebImage

class menuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, SWRevealViewControllerDelegate {

    @IBOutlet weak var eventBanner: UIImageView!
    @IBOutlet weak var tblTableView: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    
    var tilesArray = ["agenda", "access", "Event", "sponsors", "delegates", "speaker", "gallery", "CheckIn"]
    var eventData: EventData?
    var currentUser: LoginData!
    var showTiles = [String]()
    var tileImage = [String]()
    var eventId: String?
    var gradientLayer: CAGradientLayer!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
   override func viewWillLayoutSubviews() {
        imgProfile.layer.masksToBounds = false
        imgProfile.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTiles.removeAll()
        self.tileImage.removeAll()
        showTiles = ["Home".localized]
        
        tileImage = ["tile_home"]
        eventId = UserDefaults.standard.value(forKey: "EventId") as? String
        fetchUserData()
        fetchEventData()
        
    }
    
    func fetchEventData() {

        let allEventsData = MyDatabase.getProgramData(tableName: "EventData")
        if allEventsData.count > 1 {
            self.showTiles.append("Events".localized)
            self.tileImage.append("tile_events")
        }
        
        let eventsData = MyDatabase.matchProgramData(tableName: "EventData", tableId: self.eventId!, columnName: "event_id") as? [EventData]
        if let eventsData = eventsData {
            let eventData = eventsData[0]
            self.eventData = eventData
            
            if let url = self.eventData?.logo{
                if let url = URL(string: url) {
                    self.eventBanner.sd_setImage(with: url, placeholderImage: UIImage.init(named: "images.jpg"), options: .progressiveDownload, completed: nil)
                }
            }
            if eventData.agendas == "1" {
                self.showTiles.append("Agenda".localized)
                self.tileImage.append("tile_agenda")
            }

            if eventData.is_floor_plan == "1" {
                self.showTiles.append("Floor Plan".localized)
                self.tileImage.append("tile_eventfloorplan")
            }
            
            if eventData.sponsors ==  "1"{
                
                 self.showTiles.append("Sponsors".localized)
//                self.showTiles.append("Partners".localized)
                self.tileImage.append("tile_partners")
            }


            if eventData.speakers == "1" {
                self.showTiles.append("Speakers".localized)
                self.tileImage.append("tile_speakers")
            }
            
            if eventData.gallery == "1" {
                self.showTiles.append("Gallery".localized)
                self.tileImage.append("tile_gallery")
            }
            if eventData.gallery == "1" {
                self.showTiles.append("Documents".localized)
                self.tileImage.append("tile_documents")
            }
            if eventData.challenge == "1" {
                self.showTiles.append("Challenge".localized)
                self.tileImage.append("tile_challange")
            }

        }
        
        if let role = currentUser.role, role != "normal" {
            self.showTiles.append("Check In".localized)
            self.tileImage.append("tile_checkIn")
        }
        self.showTiles.append("Feedback".localized)
        self.tileImage.append("FeedBack")
        self.showTiles.append("Profile".localized)
        self.tileImage.append("tile_profile")
        self.showTiles.append("Logout".localized)
        self.tileImage.append("tile_logout")
        setUpApp()
    }
    
    func fetchUserData() {
        let userID = UserDefaults.standard.value(forKey: "UUID") as! String
        
        print("user id", userID)
        
        let loginData = MyDatabase.matchProgramData(tableName: "LoginData", tableId: userID
            , columnName: "user_id") as? [LoginData]
        currentUser = loginData![0]
        
        emailLabel.text = currentUser.email
        userNameLabel.text = currentUser.first_name! + " " + currentUser.last_name!
        let avatarURL = currentUser.avatar ?? ""
        imgProfile.sd_setShowActivityIndicatorView(true)
        imgProfile.sd_setIndicatorStyle(.gray)
        imgProfile.sd_setImage(with: URL(string: avatarURL), placeholderImage: UIImage(named: "profile.png"), options: .progressiveDownload, completed: nil)

    }

    @IBAction func showProfile(_ sender: UIButton) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        viewController.user  = currentUser
        showViewController(vc: viewController)
    }
    
    func setUpApp() {
        if gradientLayer == nil {
            gradientLayer = CAGradientLayer()
            
            gradientLayer.colors = [UIColor.clear.cgColor, UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.8).cgColor]
            gradientLayer.locations = [0, 1.0]
            gradientLayer.frame = CGRect.init(x: 0, y: 0, width: ScreenWidth, height: 130)
            self.eventBanner.layer.insertSublayer(gradientLayer, below: self.imgProfile.layer)
        }
        self.tblTableView.reloadData()

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height*0.08;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.showTiles.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        let color: UIColor!
        
        if let appColor = self.eventData?.app_color{
            color = UIColor.colorWithHexString(hex: appColor)
            //            self.tblTableView.backgroundColor = colorValue
        }else{
            color = UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
        }

        cell.backgroundColor  =  .clear
        cell.selectionStyle   =  .none
        cell.lblMenuname.text! = self.showTiles[indexPath.row]
        cell.lblMenuname.textColor = color
//        let imageName = self.tileImage[indexPath.row]
        let imageName = self.tileImage[indexPath.row]
        let originalImage = UIImage(named: imageName)
        let templateImage = originalImage!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        cell.imgIcon.image = templateImage
        cell.imgIcon.tintColor = color

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell:MenuCell = tableView.cellForRow(at: indexPath) as! MenuCell
        

        switch cell.lblMenuname.text! {
        case "Home":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
            viewController.eventId  = eventId!
            viewController.eventData = eventData
            showViewController(vc: viewController)
        case "Events":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "EventListVc") as! EventListVc
            showViewController(vc: viewController)
        case "Agenda":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "AgendaListViewController") as! AgendaListViewController
            viewController.eventData  = eventData!
            showViewController(vc: viewController)

//            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "AgendaListingVc") as! AgendaListingVc
//            viewController.eventData  = eventData!
//            showViewController(vc: viewController)
        case "Floor Plan":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultiFloorPlanViewController") as! MultiFloorPlanViewController
            viewController.eventData  = eventData!
            showViewController(vc: viewController)
        case "Sponsors":
//        case "Partners":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "Delegates_Vc") as! Delegates_Vc
            viewController.eventData  = eventData!
            showViewController(vc: viewController)
        case "Speakers":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "Spekers_VC") as! Spekers_VC
            viewController.eventData  = eventData!
            showViewController(vc: viewController)
        case "Gallery":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "PhotoGallaryVc") as! PhotoGallaryVc
            viewController.eventData  = eventData!
            showViewController(vc: viewController)
        case "Check In":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckInVc") as! CheckInVc
            viewController.eventData  = eventData!
            showViewController(vc: viewController)
        case "Documents":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DocumentListViewController") as! DocumentListViewController
            viewController.eventData  = eventData!
            showViewController(vc: viewController)
        case "Profile":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            viewController.user  = currentUser
            showViewController(vc: viewController)
        case "Logout":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            Utilities.logoutUser()
            showViewController(vc: viewController)
        case "Challenge":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ChallengesNLeaderboardVC") as! ChallengesNLeaderboardVC
            viewController.eventData  = eventData!
            showViewController(vc: viewController)
            //Challenge
            
        case "Feedback":
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedBackController") as! FeedBackController
//            viewController.eventData  = eventData!
            showViewController(vc: viewController)
        //Feedback
        default:
            break
        }
        
    }

    func showViewController(vc: UIViewController) {
        let frontVC = revealViewController().frontViewController as? UINavigationController
        frontVC?.pushViewController(vc, animated: false)
        revealViewController().pushFrontViewController(frontVC, animated: true)
    }
    
    
    static func navigateToViewController(vc: UIViewController) {
        let revealViewController = (UIApplication.shared.delegate as! AppDelegate).revealViewController
        let frontVC = revealViewController.frontViewController as? UINavigationController
        frontVC?.pushViewController(vc, animated: false)
//        revealViewController.pushFrontViewController(frontVC, animated: true)
    }
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
    }
    
}
