//
//  ViewController.swift
//  ToolApp
//
//  Created by Zaman Meraj on 26/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import JSSAlertView


class ViewController: BaseClassVC, UITextFieldDelegate {
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var tfView1: UIView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var rememberEmailLbl: UILabel!
    @IBOutlet weak var checkBtnView1: UIView!
    @IBOutlet weak var checkBtnEmail: UIButton!
    @IBOutlet weak var tfView2: UIView!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var rememberPasswordLbl: UILabel!
    @IBOutlet weak var checkBtnView2: UIView!
    @IBOutlet weak var checkBtnPaasword: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var loginWithLinkedInBtn: UIButton!
    @IBOutlet weak var notRegisteredBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    var emailSel = 0
    var passSel  = 0
    
    
    /*
    private let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81th29qe47gcye", clientSecret: "0bIpOqJW1bZ2TMD8", state: "DLKDJF46ikMMZADfdfds", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://github.com/tonyli508/LinkedinSwift"))
    */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkBtnEmail.setImage(UIImage(named: "square.png"), for: .normal)
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        self.navigationController?.navigationBar.isHidden = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tapGesture)
        if let remEmail = UserDefaults.standard.value(forKey: "rem_email") as? Int{
            if remEmail == 1{
                self.emailTF.text  =  UserDefaults.standard.value(forKey: "email") as? String
                self.checkBtnEmail.setImage(UIImage(named: "square.png"), for: .normal)
                self.checkBtnEmail.backgroundColor = .clear
            }else{
                self.emailTF.text  =  ""
                self.checkBtnEmail.setImage(UIImage(named:"unchecked.png"), for: .normal)
                self.checkBtnEmail.backgroundColor = .clear
            }
        }else{
            self.checkBtnEmail.setImage(UIImage(named:"unchecked.png"), for: .normal)
            self.checkBtnEmail.backgroundColor = .clear
            self.emailTF.text  =  ""
        }
        if let remPass = UserDefaults.standard.value(forKey: "rem_pass") as? Int{
            if remPass == 1{
                self.passwordTF.text  =  UserDefaults.standard.value(forKey: "pass") as? String
                self.checkBtnPaasword.setImage(UIImage(named: "square.png"), for: .normal)
                self.checkBtnPaasword.backgroundColor = .clear
            }else{
                self.passwordTF.text  =  UserDefaults.standard.value(forKey: "pass") as? String
                self.checkBtnPaasword.setImage(UIImage(named:"unchecked.png"), for: .normal)
                self.checkBtnPaasword.backgroundColor = .clear
            }
        }else{
            self.passwordTF.text  =  UserDefaults.standard.value(forKey: "pass") as? String
            self.checkBtnPaasword.setImage(UIImage(named:"unchecked.png"), for: .normal)
            self.checkBtnPaasword.backgroundColor = .clear
        }
//        self.emailTF.text    = "event@arhcasia.com"
  //      self.passwordTF.text = "planitswiss"
//        self.emailTF.text    = "renendra@hipster-inc.com"
//        self.passwordTF.text = "12345678"
    }
    
   @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.setTextField()
        self.setButtonProperty()
    }
    
    
    func setTextField() {
        self.passwordTF.delegate = self
        self.emailTF.delegate = self
        self.passwordTF.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
        self.emailTF.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
        self.passwordTF.isSecureTextEntry = true
    }
    
    func setButtonProperty() {
        self.loginBtn.addTarget(self, action: #selector(self.loginBtnTapped), for: .touchUpInside)
    }
    
    
    @IBAction func linkedinButtonTapped(_ sender: UIButton) {
        
        
        
        if let appUrl = URL(string: "linkedin://app"),
            UIApplication.shared.canOpenURL(appUrl) {
            navigateToLinkedInApp()
        } else {
            print(false)

            let webViewCnt = self.storyboard!.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            webViewCnt.linkedInDelegate = self
            self.navigationController?.pushViewController(webViewCnt, animated: true)
        }
    }
    
    func navigateToLinkedInApp() {
        LISDKSessionManager.clearSession()
        if LISDKSessionManager.hasValidSession() {
            handleLinkedInLogin()
        } else {
            let permissions = [LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION]
            
            LISDKSessionManager.createSession(withAuth: permissions,
                                              state: "RandomState",
                                              showGoToAppStoreDialog: true,
                                              successBlock: { (success) in
                                                self.handleLinkedInLogin()
            }) { (error) in
                print(error.debugDescription)
            }
        }
    }
    
    func handleLinkedInLogin() {
        
        if LISDKSessionManager.hasValidSession() {
            self.showHUD(forView: self.view, excludeViews: [])

            LISDKAPIHelper.sharedInstance().apiRequest("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,email-address)?format=json",
                                                       method: "GET",
                                                       body: nil, success: { (response) in
                                                        self.hideHUD(forView: self.view)
                                                        if response?.statusCode == 200 {
                                                            let dataString = response!.data
                                                            if let data = dataString?.data(using: String.Encoding.utf8) {
                                                                do {
                                                                    let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                                                                    if let myDictionary = dictionary {
                                                                        let linkedInUser = LinkedInUser(responseDict: myDictionary)
                                                                        self.linkedInLogin(linkedInUser: linkedInUser)
                                                                        
                                                                    }
                                                                } catch let error as NSError {
                                                                    print(error)
                                                                }
                                                            }
                                                        } else {
                                                            print("Error")
                                                        }
            }, error: { (error) in
                self.hideHUD(forView: self.view)
            })
        }
    }
    
    
    func linkedInLogin(linkedInUser: LinkedInUser) {

        let param: NSDictionary = ["email" : linkedInUser.email,
                                   "linkedin_id" : linkedInUser.id,
                                   "first_name": linkedInUser.firstName,
                                   "last_name": linkedInUser.lastName,
                                   "api_token": apiToken,
                                   "password": Utilities.randomString(length: 12)]
        
        self.showHUD(forView: self.view, excludeViews: [])
        
        _ = CallApi.getData(url: "http://yourevent2go.com/api/users", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
            self.hideHUD(forView: self.view)
            
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                
                self.hideHUD(forView: self.view)
                if let JSON = netResponse.responseDict as? NSDictionary {
                    self.handleLoginData(JSON: JSON)
                } else {
                    var message = "Something didn't go as expected"
                    if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                        message = errorMessage
                    }
                    DispatchQueue.main.async {
                        JSSAlertView().danger(
                            self,
                            title: "Error!".localized,
                            text: message,
                            buttonText: "Ok".localized)
                    }
                }
            } else {
                var message = "Something didn't go as expected"
                if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                    message = errorMessage
                }
                DispatchQueue.main.async {
                    JSSAlertView().danger(
                        self,
                        title: "Error!".localized,
                        text: message,
                        buttonText: "Ok".localized)
                }
                
                self.hideHUD(forView: self.view)
            }
        }
    }
    
    @objc func loginBtnTapped() {
        if self.emailTF.text == ""{
            self.view.makeToast("Email field can't be blank".localized, duration: 2, position: CSToastPositionCenter)
        }else if self.passwordTF.text == ""{
            self.view.makeToast("Password field can't be blank".localized, duration: 2, position: CSToastPositionCenter)
        }else{
            self.normalLogin()
        }
    }
    
    func normalLogin() {
        let param: NSDictionary = ["email" : self.emailTF.text ?? "",
                                   "password" : self.passwordTF.text ?? "",
                                   "api_token": apiToken]
        self.showHUD(forView: self.view, excludeViews: [])
        _ = CallApi.getData(url: "http://yourevent2go.com/api/users/authenticate_user", parameter: param as? Dictionary<String, Any>, type: .post) { (netResponse) -> (Void) in
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                self.hideHUD(forView: self.view)
                if let JSON = netResponse.responseDict as? NSDictionary {
                    self.handleLoginData(JSON: JSON)
                } else {
                    var message = "Something didn't go as expected".localized
                    if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                        message = errorMessage
                    }
                    DispatchQueue.main.async {
                        JSSAlertView().danger(
                            self,
                            title: "Error!".localized,
                            text: message,
                            buttonText: "Ok".localized)
                    }
                }
            }else {
                var message = "Something didn't go as expected".localized
                if let JSON = netResponse.responseDict as? NSDictionary, let errorMessage = JSON.value(forKey: "message") as? String {
                    message = errorMessage
                }
                DispatchQueue.main.async {
                    JSSAlertView().danger(
                        self,
                        title: "Error!".localized,
                        text: message,
                        buttonText: "Ok".localized)
                }

                self.hideHUD(forView: self.view)
            }

        }
        
    }
    
    func handleLoginData(JSON: NSDictionary) {
        MyDatabase.clearAllData()
        let loginModal = LoginModal(responseDict: JSON)
        MyDatabase.saveLoginDataLocally(loginModal: loginModal)
        
        guard let events = JSON.value(forKey: "event") as? [NSDictionary] else { return }
        
        if events.count > 0 {
            if self.checkBtnEmail.currentImage == UIImage(named: "square.png") {
                UserDefaults.standard.set(self.emailTF.text, forKey: "email")
                UserDefaults.standard.set(1, forKey: "rem_email")
                
            }else{
                UserDefaults.standard.set("", forKey: "email")
                UserDefaults.standard.set(0, forKey: "rem_email")
            }
            if self.checkBtnPaasword.currentImage == UIImage(named: "square.png") {
                UserDefaults.standard.set(self.passwordTF.text, forKey: "pass")
                UserDefaults.standard.set(1, forKey: "rem_pass")
            }else{
                UserDefaults.standard.set("", forKey: "pass")
                UserDefaults.standard.set(0, forKey: "rem_pass")
            }
            UserDefaults.standard.set("userRegister", forKey: "Registered")
            
            if events.count == 1 {
                
                let event = events[0]
                
                let eventModal = EventModal(responseDict: event)
                MyDatabase.saveEventLocally(eventModal: eventModal)
                if let banners = event.value(forKey: "banners") as? [NSDictionary] {
                    
                    for banner in banners {
                        let bannerModal = BannersModal(responseDict: banner, eventId: eventModal.eventId!)
                        MyDatabase.saveBannerLocally(bannerModal: bannerModal)
                        
                    }
                }
                
                if let pages = event.value(forKey: "pages") as? [NSDictionary] {
                    
                    for page in pages {
                        let pageModal = PageModal(responseDict: page, eventId: eventModal.eventId!)
                        MyDatabase.savePageLocally(pageModal: pageModal)
                        
                    }
                }
                
                
                self.navigateToEventsPage()
            } else {
                for event in events {
                    let eventModal = EventModal(responseDict: event)
                    MyDatabase.saveEventLocally(eventModal: eventModal)
                    if let banners = event.value(forKey: "banners") as? [NSDictionary] {
                        for banner in banners {
                            let bannerModal = BannersModal(responseDict: banner, eventId: eventModal.eventId!)
                            MyDatabase.saveBannerLocally(bannerModal: bannerModal)
                        }
                        if let pages = event.value(forKey: "pages") as? [NSDictionary] {
                            
                            for page in pages {
                                let pageModal = PageModal(responseDict: page, eventId: eventModal.eventId!)
                                MyDatabase.savePageLocally(pageModal: pageModal)
                                
                            }
                        }
                    }
                }
                self.navigateToEventsPage()
                
            }
        }
    }
    
    func navigateToEventsPage() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let frontviewcontroller = storyboard.instantiateViewController(withIdentifier: "EventListVc") as! EventListVc
        let objNavigationController : UINavigationController    =   UINavigationController.init(rootViewController: frontviewcontroller)
        let rearViewController = storyboard.instantiateViewController(withIdentifier: "menuViewController") as? menuViewController
        let mainRevealController = SWRevealViewController()
        mainRevealController.frontViewController = objNavigationController
        mainRevealController.rearViewController = rearViewController
        (UIApplication.shared.delegate as! AppDelegate).window!.rootViewController = mainRevealController
        (UIApplication.shared.delegate as! AppDelegate).window?.makeKeyAndVisible()
    }
    
    @IBAction func rem_EmailBtn(_ sender: Any) {
        if self.checkBtnEmail.currentImage == UIImage(named: "square.png") {
            self.checkBtnEmail.setImage(UIImage(named: "unchecked.png"), for: .normal)
            self.checkBtnEmail.backgroundColor = .clear
        }else{
            let img  = UIImage(named: "square.png")
            self.checkBtnEmail.setImage(img, for: .normal)
            self.checkBtnEmail.backgroundColor = .clear
        }
    }
    @IBAction func rem_PassBtn(_ sender: Any) {
        if self.checkBtnPaasword.currentImage == UIImage(named: "square.png") {
            self.checkBtnPaasword.setImage(UIImage(named: "unchecked.png"), for: .normal)
            self.checkBtnPaasword.backgroundColor = .clear
        }else{
            self.checkBtnPaasword.setImage(UIImage(named: "square.png"), for: .normal)
            self.checkBtnPaasword.backgroundColor = .clear
        }
    }
    @IBAction func forgotBtn(_ sender: Any) {
        let vc : ForgotVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotVC") as! ForgotVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func rememberPasswordCheckBtnTapped(sender: UIButton) {
        
    if self.checkBtnPaasword.currentImage == UIImage(named: "square.png") {
        self.checkBtnPaasword.setImage(UIImage(named: "unchecked.png"), for: .normal)
        self.checkBtnPaasword.backgroundColor = .clear
    }else{
        self.checkBtnPaasword.setImage(UIImage(named: "square.png"), for: .normal)
        self.checkBtnPaasword.backgroundColor = .clear
    }
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        self.checkBtnView1.layer.borderColor = UIColor.white.cgColor
//        self.checkBtnView1.layer.borderWidth = 2
//
//        self.checkBtnView2.layer.borderColor = UIColor.white.cgColor
//        self.checkBtnView2.layer.borderWidth = 2
//
        self.tfView1.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor//#colorLiteral(red: 0.3490196078, green: 0.3490196078, blue: 0.3490196078, alpha: 1).cgColor
        self.tfView1.layer.borderWidth = 1
        
        self.tfView2.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        self.tfView2.layer.borderWidth = 1
        
//        self.checkBtnView1.backgroundColor = .clear
//        self.checkBtnView2.backgroundColor = .clear
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension UIColor {
    
    static func colorWithHexString(hex: String) -> UIColor {
        var hexString = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if hexString.hasPrefix("#") {
            hexString = hexString.substring(from: String.Index.init(encodedOffset: 1))
        }
        
        if hexString.characters.count != 6 {
            return UIColor.gray
        }
        
        let rString = hexString.substring(to: String.Index.init(encodedOffset: 2))
        let gString = hexString.substring(from: String.Index.init(encodedOffset: 2)).substring(to: String.Index.init(encodedOffset: 2))
        let bString = hexString.substring(from: String.Index.init(encodedOffset: 4)).substring(to: String.Index.init(encodedOffset: 2))
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        
        
        Scanner.init(string: rString).scanHexInt32(&r)
        Scanner.init(string: gString).scanHexInt32(&g)
        Scanner.init(string: bString).scanHexInt32(&b)
//        if (countElements(hexString) != 6) {
//            return UIColor.gray
//        }
        
        return UIColor(red: CGFloat(Float(r) / 255.0), green: CGFloat(Float(g) / 255.0), blue: CGFloat(Float(b) / 255.0), alpha: CGFloat(Float(1)))
        
    }
    
    
}

extension ViewController: LinkedInLoginDelegate {
    
    func successfullyFetchedAccessToken(_ token: String) {

        let targetURLString = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,email-address)?format=json"
        
        // Initialize a mutable URL request object.
        var request = URLRequest.init(url: URL.init(string: targetURLString)!)
        
        // Indicate that this is a GET request.
        request.httpMethod = "GET"
        
        // Add the access token as an HTTP header field.
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        // Initialize a NSURLSession object.
        let session = URLSession(configuration: .default)
        
        // Make the request.
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            let statusCode = (response as! HTTPURLResponse).statusCode
            if statusCode == 200 {
                // Convert the received JSON data into a dictionary.
                do {
                    let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                    if let myDictionary = dataDictionary  as? [String: AnyObject] {
                        let linkedInUser = LinkedInUser(responseDict: myDictionary)
                        self.linkedInLogin(linkedInUser: linkedInUser)
                    }
                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                }
            } else {
                
            }
        }
        
        task.resume()

    }
}




