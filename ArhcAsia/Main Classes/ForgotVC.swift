//
//  ForgotVC.swift
//  ToolApp
//
//  Created by mayank on 06/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import JSSAlertView

class ForgotVC: BaseClassVC {
    
    @IBOutlet weak var mailField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        self.mailField.layer.borderWidth  =   1
        self.mailField.layer.borderColor  =    #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        self.mailField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
        self.mailField.setLeftPaddingPoints(10)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resetBtn(_ sender: Any) {
        view.endEditing(true)
        if let text = self.mailField.text{
            if text.isEmpty {
                _ = JSSAlertView().warning(
                    self,
                    title: AppName,
                    text: "Email Field can not be blank.".localized,
                    buttonText: "Ok".localized)
            }else{
                self.resetPassword()
            }
        }else{
            _ = JSSAlertView().warning(
                self,
                title: AppName,
                text: "Email Field can not be blank.".localized,
                buttonText: "Ok".localized)
        }
        
    }

    func resetPassword() {
        
        self.showHUD(forView: self.view, excludeViews: [])
        _ = CallApi.getData(url: "http://yourevent2go.com/api/users/request_password_reset?api_token=\(apiToken)&email=\(self.mailField.text ?? "")", parameter: nil, type: .get) { (netResponse) -> (Void) in
            self.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    var message = "A new password reset link has been sent to your email.".localized
                    if let JSON = netResponse.responseDict as? NSDictionary, let msg = JSON.value(forKey: "message") as? String {
                        message = msg
                    }
                    JSSAlertView().success(
                        self,
                        title: AppName,
                        text: message,
                        buttonText: "Ok".localized)

                } else {
                    var message = "Something didn't go as expected.".localized
                    if let JSON = netResponse.responseDict as? NSDictionary, let msg = JSON.value(forKey: "message") as? String {
                        message = msg
                    }
                    JSSAlertView().danger(
                        self,
                        title: "Error!".localized,
                        text: message,
                        buttonText: "Ok".localized)
                }
            }
        }
    }
}
extension UITextField{
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
