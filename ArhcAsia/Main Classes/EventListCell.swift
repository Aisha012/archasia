//
//  EventListCell.swift
//  ToolApp
//
//  Created by mayank on 16/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit

class EventListCell: UITableViewCell {
    @IBOutlet weak var iamgeName: UIImageView!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var holderView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 4
        self.holderView.layer.cornerRadius = 5
        self.holderView.layer.shadowOpacity = 0.3
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
