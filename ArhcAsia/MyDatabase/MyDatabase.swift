//
//  MyDatabase.swift
//  ToolApp
//
//  Created by Zaman Meraj on 03/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import Foundation
import CoreData

class MyDatabase {
    
    static func saveLoginDataLocally(loginModal: LoginModal) {
        var context: NSManagedObjectContext!
        
        if #available(iOS 10.0, *) {
            context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let entity              =   NSEntityDescription.entity(forEntityName: "LoginData",in: context)!
//            var  managedObject      =   LoginData(entity: entity, insertInto: context)
            let checkData = MyDatabase.matchProgramData(tableName: "LoginData", tableId: loginModal.userId!, columnName: "user_id")
            if checkData.count > 0{
                let managedObject            =   checkData[0] as! LoginData
                MyDatabase.populateLoginDataModal(managedObj: managedObject, model: loginModal)
            }else{
               let managedObject            =   NSManagedObject(entity: entity,insertInto: context) as! LoginData
                MyDatabase.populateLoginDataModal(managedObj: managedObject, model: loginModal)
            }
            
            do {
                try context!.save()
            }catch let err {
                print(err.localizedDescription)
            }
            
        } else {
            context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
            let entity              =   NSEntityDescription.entity(forEntityName: "LoginData",in: context!)!
            var  managedObject     : NSManagedObject?
            let checkData = MyDatabase.matchProgramData(tableName: "LoginData", tableId: loginModal.userId!, columnName: "user_id")
            
            if checkData.count > 0{
                managedObject            =   checkData[0]
            }else{
                managedObject            =   NSManagedObject(entity: entity,insertInto: context!)
            }
            
            managedObject?.setValue(loginModal.authenticate_user, forKey: "authenticate_user")
            managedObject?.setValue(loginModal.avatar, forKey: "avatar")
            managedObject?.setValue(loginModal.email, forKey: "email")
            managedObject?.setValue(loginModal.first_name, forKey: "first_name")
            managedObject?.setValue(loginModal.last_name, forKey: "last_name")
            managedObject?.setValue(loginModal.mobile, forKey: "mobile")
            managedObject?.setValue(loginModal.qrCode, forKey: "qrcode")
            managedObject?.setValue(loginModal.role, forKey: "role")
            managedObject?.setValue(loginModal.userId, forKey: "user_id")
            managedObject?.setValue(loginModal.position, forKey: "position")
            managedObject?.setValue(loginModal.company, forKey: "company")
            UserDefaults.standard.setValue(loginModal.userId, forKey: "userID")
            UserDefaults.standard.synchronize()
            do {
                try context!.save()
            }catch let err {
                print(err.localizedDescription)
            }
        }
        
        
    }
    
    static func populateLoginDataModal(managedObj: LoginData, model: LoginModal) {
        managedObj.authenticate_user = model.authenticate_user
        managedObj.avatar            = model.avatar
        managedObj.email             = model.email
        managedObj.first_name        = model.first_name
        managedObj.last_name         = model.last_name
        managedObj.mobile            = model.mobile
        managedObj.qrcode            = model.qrCode
        managedObj.role              = model.role
        managedObj.user_id           = model.userId
        managedObj.position          = model.position
        managedObj.company           = model.company

    }

    
   static func saveEventLocally(eventModal: EventModal) {

    var context: NSManagedObjectContext!
        
        if #available(iOS 10.0, *) {
            context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let entity              =   NSEntityDescription.entity(forEntityName: "EventData",in: context!)!
//            var  managedObject      =   EventData(entity: entity, insertInto: context)
            let checkData = MyDatabase.matchProgramData(tableName: "EventData", tableId: eventModal.eventId!, columnName: "event_id")
            if checkData.count > 0{
                let managedObject            =   checkData[0] as! EventData
                MyDatabase.populateEventDataModal(managedObj: managedObject, model: eventModal)
            }else{
                let managedObject            =   NSManagedObject(entity: entity,insertInto: context!) as! EventData
                MyDatabase.populateEventDataModal(managedObj: managedObject, model: eventModal)
            }
            
            do {
                try context!.save()
            }catch let err {
                print(err.localizedDescription)
            }
            
        } else {
            context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
            let entity              =   NSEntityDescription.entity(forEntityName: "EventData",in: context!)!
            var  managedObject     : NSManagedObject?
            let checkData = MyDatabase.matchProgramData(tableName: "EventData", tableId: eventModal.eventId!, columnName: "event_id")
            
            if checkData.count > 0 {
                managedObject            =   checkData[0]
            }else{
                managedObject            =   NSManagedObject(entity: entity,insertInto: context!)
            }
            
            managedObject?.setValue(eventModal.appColor, forKey: "app_color")
            managedObject?.setValue(eventModal.appToken, forKey: "app_token")
            managedObject?.setValue(eventModal.eventDescription, forKey: "event_description")
            managedObject?.setValue(eventModal.eventId, forKey: "event_id")
            managedObject?.setValue(eventModal.eventLogo, forKey: "logo")
            managedObject?.setValue(eventModal.eventName, forKey: "event_nmae")
            managedObject?.setValue(eventModal.is_floor_plan, forKey: "is_floor_plan")
            managedObject?.setValue(eventModal.is_floor_plan, forKey: "floor_plan")
            managedObject?.setValue(eventModal.gallery, forKey: "gallery")
            managedObject?.setValue(eventModal.speaker, forKey: "speakers")
            managedObject?.setValue(eventModal.sponsors, forKey: "sponsors")
            managedObject?.setValue(eventModal.startTime, forKey: "start_time")
            managedObject?.setValue(eventModal.endTime, forKey: "end_time")
            managedObject?.setValue(eventModal.agendas, forKey: "agendas")
            managedObject?.setValue(eventModal.youtube, forKey: "youtube")
            managedObject?.setValue(eventModal.document, forKey: "document")
            managedObject?.setValue(eventModal.chat, forKey: "chat")
            managedObject?.setValue(eventModal.allowShare, forKey: "allow_share")
            managedObject?.setValue(eventModal.allowUpload, forKey: "allow_upload")
            //changes done by deepak
            managedObject?.setValue(eventModal.challenge, forKey: "challenge")


            do {
                try context!.save()
            }catch let err {
                print(err.localizedDescription)
            }
        }
    }
    
    static func populateEventDataModal(managedObj: EventData, model: EventModal) {
        managedObj.app_color = model.appColor
        managedObj.app_token = model.appToken
        managedObj.event_description = model.eventDescription
        managedObj.event_id = model.eventId
        managedObj.event_name = model.eventName
        managedObj.is_floor_plan = model.is_floor_plan
        managedObj.floor_plan = model.floor_plan
        managedObj.agendas = model.agendas
        managedObj.gallery = model.gallery
        managedObj.logo = model.eventLogo
        managedObj.speakers = model.speaker
        managedObj.sponsors = model.sponsors
        managedObj.start_time = model.startTime
        managedObj.end_time = model.endTime
        managedObj.youtube = model.youtube
        managedObj.document = model.document
        managedObj.chat = model.chat
        managedObj.challenge = model.challenge
        managedObj.allow_share = model.allowShare
        managedObj.allow_upload = model.allowUpload

        
    }

    static func matchProgramData(tableName: String, tableId : String, columnName: String) -> [NSManagedObject]{
        var newArr                  =   [NSManagedObject]()
        if #available(iOS 10.0, *) {
            let appDelegate         =   (UIApplication.shared.delegate) as! AppDelegate
            let context             =   appDelegate.persistentContainer.viewContext
            let request             =   NSFetchRequest<NSFetchRequestResult>(entityName:tableName)
            request.returnsObjectsAsFaults = false
            let checkPredicate      =   NSPredicate(format: "\(columnName) ==  \(tableId)")
            request.predicate       =   checkPredicate;
            newArr                  =   try! context.fetch(request) as! [NSManagedObject]
            return newArr
        } else {
            // Fallback on earlier versions
            return newArr
        }
    }
    
    
    static func getProgramData(tableName: String) -> [NSManagedObject]{
        var newArr                  =   [NSManagedObject]()
        if #available(iOS 10.0, *) {
            let appDelegate         =   (UIApplication.shared.delegate) as! AppDelegate
            let context             =   appDelegate.persistentContainer.viewContext
            let request             =   NSFetchRequest<NSFetchRequestResult>(entityName:tableName)
            request.returnsObjectsAsFaults = false
            newArr                  =   try! context.fetch(request) as! [NSManagedObject]
            return newArr
        } else {
            // Fallback on earlier versions
            return newArr
        }
    }
    
    static func saveBannerLocally(bannerModal: BannersModal) {

        var context: NSManagedObjectContext!
        
        if #available(iOS 10.0, *) {
            context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let entity              =   NSEntityDescription.entity(forEntityName: "BannerData",in: context!)!

            let checkData = MyDatabase.matchProgramData(tableName: "BannerData", tableId: bannerModal.bannerId!, columnName: "banner_id")
            if checkData.count > 0{
                let managedObject            =   checkData[0] as! BannerData
                MyDatabase.populateBannerDataModal(managedObj: managedObject, model: bannerModal)
            }else{
                let managedObject            =   NSManagedObject(entity: entity,insertInto: context!) as! BannerData
                MyDatabase.populateBannerDataModal(managedObj: managedObject, model: bannerModal)
            }
            
            do {
                try context!.save()
            }catch let err {
                print(err.localizedDescription)
            }
            
        } else {
            context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
            let entity              =   NSEntityDescription.entity(forEntityName: "BannerData",in: context!)!
            var  managedObject     : NSManagedObject?
            let checkData = MyDatabase.matchProgramData(tableName: "BannerData", tableId: bannerModal.bannerId!, columnName: "banner_id")
            if checkData.count > 0{
                managedObject            =   checkData[0]
            }else{
                managedObject            =   NSManagedObject(entity: entity,insertInto: context!)
            }
            
            managedObject?.setValue(bannerModal.bannerDescription, forKey: "banner_description")
            managedObject?.setValue(bannerModal.bannerId, forKey: "banner_id")
            managedObject?.setValue(bannerModal.bannerImage, forKey: "banner_image")
            managedObject?.setValue(bannerModal.bannerTitle, forKey: "banner_title")
            managedObject?.setValue(bannerModal.eventId, forKey: "event_id")
            managedObject?.setValue(bannerModal.bannerVideo, forKey: "banner_video")
            do {
                try context!.save()
            }catch let err {
                print(err.localizedDescription)
            }
        }
        
    }
   
    static func populateBannerDataModal(managedObj: BannerData, model: BannersModal) {
        managedObj.banner_description = model.bannerDescription
        managedObj.banner_id = model.bannerId
        managedObj.banner_image = model.bannerImage
        managedObj.banner_title = model.bannerTitle
        managedObj.event_id = model.eventId
        managedObj.banner_video = model.bannerVideo
    }
    
    static func savePageLocally(pageModal: PageModal) {
        
        var context: NSManagedObjectContext!
        
        if #available(iOS 10.0, *) {
            context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let entity              =   NSEntityDescription.entity(forEntityName: "PageData",in: context!)!
            
            let checkData = MyDatabase.matchProgramData(tableName: "PageData", tableId: pageModal.pageId!, columnName: "page_id")
            if checkData.count > 0{
                let managedObject            =   checkData[0] as! PageData
                MyDatabase.populatePageDataModal(managedObj: managedObject, model: pageModal)
            }else{
                let managedObject            =   NSManagedObject(entity: entity,insertInto: context!) as! PageData
                MyDatabase.populatePageDataModal(managedObj: managedObject, model: pageModal)
            }
            
            do {
                try context!.save()
            }catch let err {
                print(err.localizedDescription)
            }
            
        } else {
            context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
            let entity              =   NSEntityDescription.entity(forEntityName: "PageData",in: context!)!
            var  managedObject     : NSManagedObject?
            let checkData = MyDatabase.matchProgramData(tableName: "PageData", tableId: pageModal.pageId!, columnName: "page_id")
            if checkData.count > 0{
                managedObject            =   checkData[0]
            }else{
                managedObject            =   NSManagedObject(entity: entity,insertInto: context!)
            }
            
            managedObject?.setValue(pageModal.pageId, forKey: "page_id")
            managedObject?.setValue(pageModal.pageName, forKey: "name")
            managedObject?.setValue(pageModal.pageId, forKey: "desc")
            managedObject?.setValue(pageModal.eventId, forKey: "event_id")
            do {
                try context!.save()
            }catch let err {
                print(err.localizedDescription)
            }
        }
        
    }
    
    static func populatePageDataModal(managedObj: PageData, model: PageModal) {
        managedObj.page_id = model.pageId
        managedObj.name = model.pageName
        managedObj.desc = model.pageDescription
        managedObj.event_id = model.eventId

    }


    static func clearAllData() {
        clearData(entityName: "LoginData")
        clearData(entityName: "EventData")
        clearData(entityName: "BannerData")
        clearData(entityName: "PageData")
    }
    
    static func clearData(entityName: String) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        if #available(iOS 10.0, *) {
            let context = delegate.persistentContainer.viewContext
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            
            do {
                try context.execute(deleteRequest)
                try context.save()
            } catch {
                print ("There was an error")
            }
        } else {
            let context = delegate.managedObjectContext
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            
            do {
                try context.execute(deleteRequest)
                try context.save()
            } catch {
                print ("There was an error")
            }
        }
    }
    
    
}
