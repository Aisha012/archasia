//
//  Speakers_TVC.swift
//  ToolApp
//
//  Created by Prashant Tiwari on 30/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit

class Speakers_TVC: UITableViewCell {

    @IBOutlet weak var speaker_img: UIImageView!
    @IBOutlet weak var speaker_name: UILabel!
    @IBOutlet weak var speaker_info: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
