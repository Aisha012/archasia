//
//  EventFloorPlanViewController.swift
//  ToolApp
//
//  Created by Phaninder on 04/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class EventFloorPlanViewController: BaseClassVC {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!

    var screenTitle = "Floor Plan"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")
        self.titleLabel.text = screenTitle
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
        
    }

}
