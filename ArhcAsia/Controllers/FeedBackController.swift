//
//  PhotoLoungeController.swift
//  ToolApp
//
//  Created by Renendra Saxena on 16/10/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class FeedBackController: BaseClassVC , UIWebViewDelegate{
    
    @IBOutlet var webView : UIWebView!
//    @IBOutlet var titleLabel : UILabel!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var bottomView: UIView!


    var showMenuButton = true

    var titleWebView : String?
    var urlString : String?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")
        self.titleLabel.text = "Feedback".localized
        self.navigationController?.isNavigationBarHidden = true
        let url = URL (string: "https://www.surveymonkey.com/r/YC7R69M")
        let request = URLRequest(url: url!)
        webView.loadRequest(request)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
//    @objc func navigateBack() {
//        self.navigationController?.popViewController(animated: true)
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backBtnPressed(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        activity.isHidden = false
        activity.startAnimating()
        NSLog("Webview load has started")
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activity.stopAnimating()
        activity.isHidden = true
        NSLog("Webview load had finished")
    }
    
    
}
