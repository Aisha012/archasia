//
//  SponsorDetailsViewController.swift
//  ToolApp
//
//  Created by Phaninder on 14/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import SafariServices

class SponsorDetailsViewController: BaseClassVC {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!

    @IBOutlet weak var partnerImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var descLabel: UITextView!
    
    var sponsor: Sponsor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")
        titleLabel.text = sponsor.name ?? "Speaker Details"
        
        partnerImageView.sd_setShowActivityIndicatorView(true)
        partnerImageView.sd_setIndicatorStyle(.gray)
        partnerImageView.sd_setImage(with: URL(string: sponsor.logo), placeholderImage: UIImage(named: "gallery_placeholder"), options: .progressiveDownload, completed: nil)
        categoryLabel.text = sponsor.category.isEmpty ? "NA" : sponsor.category
        websiteButton.setTitle(sponsor.website, for: .normal)
        descLabel.attributedText = sponsor.htmlInfo
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func websiteButtonTapped(_ sender: UIButton) {
        if let url = URL(string: sponsor.website) {
            if #available(iOS 11.0, *) {
                let config: SFSafariViewController.Configuration = SFSafariViewController.Configuration.init()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true, completion: nil)
            } else {
                let safariController = SFSafariViewController(url: url)
                self.present(safariController, animated: true, completion: nil)
            }
        }

    }
    
}
