//
//  AgendaListViewController.swift
//  ToolApp
//
//  Created by Phaninder on 08/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import EventKit
import JSSAlertView

class AgendaListViewController: BaseClassVC {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var Agenda = [NSDictionary]()
    var headerArray =  [String]()
    var eventId: String?
    var eventData: EventData!
    var pageIndex = 1
    var date = [String]()
    var completeModal = [CompleteAgendaModal]()
    var currentModal = [CompleteAgendaModal]()
    var callAgendaAPI = true
    var showMenuButton = true
    let store = EKEventStore()
    var canAccessCalendar = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        canAccessCalendar = EKEventStore.authorizationStatus(for: .event) == .authorized

        self.titleLabel.text = "Agenda".localized
        
        self.tableView.tableFooterView = UIView()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        
        self.eventId = eventData.event_id!
        
        if canAccessCalendar == false {
            store.requestAccess(to: .event) { (success, error) in
                
                if error == nil {
                    self.canAccessCalendar = true
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } else {
                    //we have error in getting access to device calnedar
                    print("error = \(String(describing: error?.localizedDescription))")
                }
                
                
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
        if callAgendaAPI {
            fetchAgendas()
        } else {
            callAgendaAPI = true
        }
    }

    @IBAction func navigateToFilterScreen() {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        destination.completeModal = self.completeModal
        destination.eventId = self.eventId
        destination.filterDelegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }

}

extension AgendaListViewController {
    
    func fetchAgendas() {
        
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://yourevent2go.com/api/events/\(self.eventId!)/agenda_list?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self.hideHUD(forView: self.view)
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        self.completeModal.removeAll()
                        for jsonValue in JSON {
                            let completeModal = CompleteAgendaModal(responseDic: jsonValue, eventId: self.eventId!)
                            self.completeModal.append(completeModal)
                            
                        }
                        self.currentModal = self.completeModal
                        if self.completeModal.count <= 0{
                            self.view.makeToast("There is no agenda listing".localized)
                            self.tableView.reloadData()
                        }else{
                            self.hideHUD(forView: self.view)
                            self.tableView.reloadData()
                        }
                        
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CalendarNav" {
            let calendarVC = segue.destination as! CalendarViewController
            calendarVC.completeModal = completeModal
            calendarVC.eventId = self.eventId
        }
    }
    
    func createEventInTheCalendar(with title:String, forDate eventStartDate:Date, toDate eventEndDate:Date, agendaIndex indexPath: IndexPath) {
        
        store.requestAccess(to: .event) { (success, error) in
            if  error == nil {
                self.canAccessCalendar = true
                let event = EKEvent.init(eventStore: self.store)
                event.title = title
                event.calendar = self.store.defaultCalendarForNewEvents // this will return deafult calendar from device calendars
                event.startDate = eventStartDate
                event.endDate = eventEndDate
                
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: -3600, since: event.startDate))
                event.addAlarm(alarm)
                
                do {
                    try self.store.save(event, span: .thisEvent)
                    DispatchQueue.main.async {
                        let alert = JSSAlertView().success(
                            self,
                            title: AppName,
                            text: "Added the event to the calendar".localized,
                            buttonText: "Ok".localized)
                        alert.addAction {
                            self.currentModal[indexPath.section].list[indexPath.row].isAddedToCalendar = true
                            self.tableView.reloadData()
                        }

                    }
                } catch let error as NSError {
                    print("failed to save event with error : \(error)")
                }
                
            } else {
                //we have error in getting access to device calnedar
                print("error = \(String(describing: error?.localizedDescription))")
            }
        }
    }


    func removeEventFromCalendar(with title:String, forDate eventStartDate:Date, toDate eventEndDate:Date, agendaIndex indexPath: IndexPath) {
        
        store.requestAccess(to: .event) { (success, error) in
            if  error == nil {
                self.canAccessCalendar = true
                let predicate = self.store.predicateForEvents(withStart: eventStartDate, end: eventEndDate, calendars: nil)
                let existingEvents = self.store.events(matching: predicate)
                for singleEvent in existingEvents {
                    if singleEvent.title == title && singleEvent.startDate == eventStartDate {
                        do {
                            try self.store.remove(singleEvent, span: .thisEvent)
                            DispatchQueue.main.async {
                                let alert = JSSAlertView().success(
                                    self,
                                    title: AppName,
                                    text: "Removed the event from the calendar".localized,
                                    buttonText: "Ok".localized)
                                alert.addAction {
                                    self.currentModal[indexPath.section].list[indexPath.row].isAddedToCalendar = false
                                    self.tableView.reloadData()
                                    
                                }
                            }
                            
                        } catch let error as NSError {
                            print("failed to remove event with error : \(error)")
                        }
                    }
                }
            } else {
                //we have error in getting access to device calnedar
                print("error = \(String(describing: error?.localizedDescription))")
            }
        }
    }

}

extension AgendaListViewController: UITableViewDelegate, UITableViewDataSource {

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return currentModal.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentModal[section].list.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AgendaListTableViewCell.cellIdentifier(), for: indexPath) as! AgendaListTableViewCell
        cell.delegate = self
        let agendaModel = currentModal[indexPath.section].list[indexPath.row]
        if canAccessCalendar,
            let title = agendaModel.title,
            let startDate = Utilities.getDateFromString(agendaModel.start_date),
            let endDate = Utilities.getDateFromString(agendaModel.end_date) {

            let predicate = store.predicateForEvents(withStart: startDate, end: endDate, calendars: nil)
            let existingEvents = store.events(matching: predicate)
            for singleEvent in existingEvents {
                if singleEvent.title == title && singleEvent.startDate == startDate {
                    currentModal[indexPath.section].list[indexPath.row].isAddedToCalendar = true
                }
            }
        }
        cell.configureCell(agenda: currentModal[indexPath.section].list[indexPath.row], atIndex: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        self.tableView.estimatedRowHeight = 140
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHead = UIView(frame: CGRect(x: 0, y: 0, width : self.tableView.frame.size.width, height : self.view.frame.size.height*0.5))
        /* Create custom view to display section header... */
        let label = UILabel(frame: CGRect(x :10, y : viewHead.frame.size.height*0.5/7.5, width : tableView.frame.size.width, height :  18))
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.black
        let myDate = DateExtension.changeDateFormat(date: self.currentModal[section].date!, getDateFormat: "yyyy-MM-dd", desiredDateFormat: "EEE, MMM dd yyyy")
        label.text = myDate
        viewHead.addSubview(label)
        viewHead.backgroundColor = UIColor.white
        return viewHead
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.tableView.frame.size.height*0.08
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionNo = indexPath.section
        let value = self.currentModal[sectionNo].list[indexPath.row]
        
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "AgendaSubDetailVc") as! AgendaSubDetailVc
        destination.lisModal      = value
        destination.eventId       = self.eventId
        self.navigationController?.pushViewController(destination, animated: true)

    }
    
}

extension AgendaListViewController: AgendaListTableViewCellDelegate {
    
    func scheduleButtonTapped(atIndex index: IndexPath) {
        let agenda = self.currentModal[index.section].list[index.row]
        if let title = agenda.title,
            let startDate = Utilities.getDateFromString(agenda.start_date),
            let endDate = Utilities.getDateFromString(agenda.end_date) {
            if agenda.isAddedToCalendar {
                removeEventFromCalendar(with: title, forDate: startDate, toDate: endDate, agendaIndex: index)
            } else {
                createEventInTheCalendar(with: title, forDate: startDate, toDate: endDate, agendaIndex: index)
            }
        }
        
    }
    
}

extension AgendaListViewController: FilterViewControllerDelegate {
    
    func filterApplied(_ category: String,/* room: String,*/ date: String) {
        callAgendaAPI = false
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            let hudView = MBProgressHUD.showAdded(to: view, animated: true)
            
            var url = "http://yourevent2go.com/api/events/\(self.eventId!)/agenda_list?user_token=\(auth_Token)&api_token=\(apiToken)"
            if !category.isEmpty {
                url += "&category=\(category)"
            }
//            
//            if !room.isEmpty {
//                url += "&room=\(room)"
//            }
            
            if !date.isEmpty {
                url += "&date=\(date)"
            }
            
            _ = CallApi.getData(url: url, parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    DispatchQueue.main.async {
                        hudView.hide(animated: true)
                    }
                    
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        self.currentModal.removeAll()
                        for jsonValue in JSON {
                            let completeModal = CompleteAgendaModal(responseDic: jsonValue, eventId: self.eventId!)
                            self.currentModal.append(completeModal)
                        }
                        if self.currentModal.count <= 0 {
                            self.view.makeToast("There is no agenda listing".localized)
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
        
    }
    
}
