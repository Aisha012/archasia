//
//  PollCustomPagerViewController.swift
//  ToolApp
//
//  Created by Phaninder on 17/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PollCustomPagerViewController: ButtonBarPagerTabStripViewController {

    var polls = [Poll]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(directionButtonTapped(_:)),
                                               name: .directionButtonTapped,
                                               object: nil)

    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        var viewControllerArray = [UIViewController]()
        for (index, poll) in polls.enumerated() {
            let pollVC = self.storyboard?.instantiateViewController(withIdentifier: "PollChildViewController") as! PollChildViewController
            pollVC.poll = poll
            pollVC.polls = polls
            pollVC.pageIndex = index
            viewControllerArray.append(pollVC)
        }
        return viewControllerArray
    }
    
    
    @objc func directionButtonTapped(_ notification: NSNotification) {
        if let pageNumber = notification.userInfo?["data"] as? Int {
            self.moveToViewController(at: pageNumber)
        }
    }
}
