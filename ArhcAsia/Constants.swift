//
//  Constants.swift
//  ToolApp
//
//  Created by Phaninder on 16/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

//MARK: - Dimension Constants -

let ScreenWidth = UIScreen.main.bounds.width
let ScreenHeight = UIScreen.main.bounds.height

let apiToken = "b9e01453-aca6-466d-bcfc-81af0eade79c"//"b59361eb-bb8d-42c5-aedd-053d37728711"//"113cf28b-728f-4696-8aa7-8f4365f6c475"

enum PaginationType: Int {
    case new, old, reload
}

let AppName = "Tool App".localized

extension Notification.Name {
    static let categorySelected = Notification.Name("categorySelected")
    static let roomSelected = Notification.Name("roomSelected")
    static let dateSelected = Notification.Name("dateSelected")
    static let searchChanged = Notification.Name("searchChanged")
    static let fetchSearchData = Notification.Name("fetchSearchData")
    static let questionChanged = Notification.Name("questionChanged")
    static let directionButtonTapped = Notification.Name("directionButtonTapped")
    static let questionSubmitted = Notification.Name("questionSubmitted")
}

