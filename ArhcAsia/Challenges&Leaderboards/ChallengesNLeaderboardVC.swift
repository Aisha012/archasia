//
//  ChallengesNLeaderboardVC.swift
//  ArhcAsia
//
//  Created by Namespace  on 05/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

protocol PopupDelegate : class{
    func showMessage()
}


class ChallengesNLeaderboardVC: BaseClassVC , UITableViewDataSource, UITableViewDelegate, PopupDelegate{
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var segmentedIndex: UISegmentedControl!
    @IBOutlet weak var tbleView: UITableView!
    var rowCountSectionOne :Int? = 0
    var rowCountSectionTwo:Int? = 0
    var selectedIndex = 0
    var eventData: EventData!
    var showMenuButton = true
    override func viewDidLoad() {
        super.viewDidLoad()
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        let nib = UINib(nibName: "TableSectionHeader", bundle: nil)
        tbleView.register(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
        let nib2 = UINib(nibName: "ChallengesHeader", bundle: nil)
        tbleView.register(nib2, forHeaderFooterViewReuseIdentifier: "ChallengesHeader")
        let nib3 = UINib(nibName: "TableSectionFooter", bundle: nil)
        tbleView.register(nib3, forHeaderFooterViewReuseIdentifier: "TableSectionFooter")
        self.eventId = eventData.event_id!
        let segAttributes: NSDictionary = [
            NSAttributedStringKey.foregroundColor: UIColor.white,
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)
        ]
        self.tbleView.separatorColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        segmentedIndex.setTitleTextAttributes(segAttributes as [NSObject : AnyObject], for: UIControlState.selected)
        customNavBar.backgroundColor = navigationBarColor()
        fetchChallanges()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func controllerSegmentChanged(_ sender: UISegmentedControl) {
        sleep(UInt32(0.1))
        selectedIndex = selectedIndex == 0 ? 1 : 0
        rowCountSectionOne =  0
        rowCountSectionOne = 0
        self.tbleView.separatorColor = selectedIndex == 0 ? UIColor.clear : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        if selectedIndex == 0{
            if self.chalengeModel.count > 0{
                self.rowCountSectionOne = self.chalengeModel.count
                self.tbleView.reloadData()
            }else{
                self.fetchChallanges()
            }
        }else{
            self.cCell = nil
            if self.leaderBoardResponse != nil{
                self.rowCountSectionOne = self.leaderBoardResponse?.leader_board?.count
                self.rowCountSectionTwo = self.leaderBoardResponse?.my_position?.count
                self.tbleView.reloadData()
               

            }else{
                fetchLeaderBords()
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
                self.tbleView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                
            }
        }
        
    }
    
    @IBAction func btnOnTapExpand(_ sender: UIButton) {
       // selectedIndex = selectedIndex == 0 ? 1 : 0
        rowCountSectionOne = 0//selectedIndex == 0 ? 0 : 20
        self.tbleView.reloadData()

        
    }
    @objc override func navigateBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    var isRefreshing:Bool = false
    override func refresh(_ sender: UIRefreshControl) {
        if selectedIndex == 0{
            isRefreshing = true
            self.currentPage = 1
            loadNewPageData = true
            self.fetchChallanges(refreshControler: sender)
        }else{
            sender.endRefreshing()
        }
        
    }
    
    
    func showMessage(){
        self.view.makeToast("Your Answer has been submitted successfully.")

    }
    
    
    
    //MARK:- Navigation_Bar_Color_Mthd
    override func navigationBarColor() -> UIColor {
        if let color_Value  =  UserDefaults.standard.value(forKey: "color_Value") as? String {
            return UIColor.colorWithHexString(hex: color_Value)
        }else{
            return UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // section 0 height is 138 but not to show i made it 0
        return selectedIndex == 1 ? 89 :0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return selectedIndex == 1 ? 2 :1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? (rowCountSectionOne == nil ? 0 :rowCountSectionOne)!:(rowCountSectionTwo == nil ? 0 :rowCountSectionTwo)!
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedIndex == 0{
            if loadNewPageData && tableView.contentOffset.y > 0 && indexPath.row == rowCountSectionOne! - 2{
                self.currentPage = self.currentPage + 1
                self.fetchChallanges()
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "challengeCell", for: indexPath) as! ChallengeCell
            cell.vwCorner.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.vwCorner.layer.borderWidth = 1
            cell.vwCorner.layer.cornerRadius = 5.0
            cell.lblTitle.text = self.chalengeModel[indexPath.row].title!
            cell.lblDescription.text = self.chalengeModel[indexPath.row].description!
            cell.lblPoints.text = "\(self.chalengeModel[indexPath.row].points!) Points"
        
            //cell.lblTitle.text = self.chalengeModel[indexPath.row].title!

            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableCell
        cell.vwTopLine.isHidden = indexPath.row != 0
        cell.vwBottom.layer.cornerRadius = 3
        cell.vwBottom.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        switch indexPath.section {
        case 0:
            cell.vwLeft.isHidden = indexPath.row == rowCountSectionOne! - 1
            cell.vwRight.isHidden = indexPath.row == rowCountSectionOne! - 1
            cell.vwBottom.layer.borderWidth = indexPath.row == rowCountSectionOne! - 1 ? 1 :0
            cell.lblRank.text = self.leaderBoardResponse?.leader_board?[indexPath.row].rank == nil ? "N/A" : "\(String(describing: self.leaderBoardResponse!.leader_board![indexPath.row].rank!))"
            cell.lblName.text = "\(String(describing: self.leaderBoardResponse?.leader_board?[indexPath.row].user?.first_name == nil ? "" :self.leaderBoardResponse!.leader_board![indexPath.row].user!.first_name!))" +  " \(self.leaderBoardResponse?.leader_board![indexPath.row].user?.last_name == nil ? "":self.leaderBoardResponse!.leader_board![indexPath.row].user!.last_name! ) "
            cell.lblPoints.text =  self.leaderBoardResponse?.leader_board?[indexPath.row].score != nil ? "\(self.leaderBoardResponse!.leader_board![indexPath.row].score!)" : "N/A"
            if let url = self.leaderBoardResponse?.leader_board?[indexPath.row].user?.avatar{
                let Url = URL(string: url)
                cell.ivUser.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "tile_profile"), options: .progressiveDownload, progress: nil, completed: nil)
            }

            break
        case 1:
            cell.vwLeft.isHidden = indexPath.row == rowCountSectionTwo! - 1
            cell.vwRight.isHidden = indexPath.row == rowCountSectionTwo! - 1
            cell.vwBottom.layer.borderWidth = indexPath.row == rowCountSectionTwo! - 1 ? 1 :0
            cell.lblRank.text = self.leaderBoardResponse?.my_position?[indexPath.row].rank == nil ? "N/A" : "\(self.leaderBoardResponse!.my_position![indexPath.row].rank!)"
            cell.lblName.text = "\(self.leaderBoardResponse?.my_position?[indexPath.row].user?.first_name == nil ? "" :self.leaderBoardResponse!.my_position![indexPath.row].user!.first_name!)" + " \(self.leaderBoardResponse?.my_position![indexPath.row].user?.last_name == nil ? "":self.leaderBoardResponse!.my_position![indexPath.row].user!.last_name! ) "
            cell.lblPoints.text =  self.leaderBoardResponse?.my_position?[indexPath.row].score != nil ? "\(self.leaderBoardResponse!.my_position![indexPath.row].score!)" : "N/A"
            if let url = self.leaderBoardResponse?.my_position?[indexPath.row].user?.avatar{
                let Url = URL(string:url)
                cell.ivUser.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "tile_profile"), options: .progressiveDownload, progress: nil, completed: nil)
            }
            break
        default:
            break
        }
       
       
    //9984442727

        return cell
    }
    var titles = ["Game Leaderboard", "My Position"]
    var cCell:ChallengesHeader?
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if selectedIndex == 1{
        let cell = self.tbleView.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader")
        let header = cell as! TableSectionHeader
        header.titleLabel.text = titles[section]
        header.vwTitle.layer.borderWidth  = 1.0
        header.vwTitle.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
           // header.vwTitle.roundCorners(corners: .topRight, radius: 5)
        header.vwTitle.layer.cornerRadius = 3
        header.vwTitle.clipsToBounds = true

        return cell
        }
//        if selectedIndex == 0{
//            let cell = self.tbleView.dequeueReusableHeaderFooterView(withIdentifier: "ChallengesHeader")
//            let header = cell as! ChallengesHeader
//            self.cCell = header
//            header.btnSubmit.layer.cornerRadius = 15
//            header.btnSubmit.layer.borderWidth = 1
//            header.btnSubmit.clipsToBounds = true
//            header.txtInput.layer.cornerRadius = 15
//            header.txtInput.layer.borderWidth = 1
//
//            header.txtInput.clipsToBounds = true
//            header.lblChanges.layer.cornerRadius = 10
//            header.lblChanges.clipsToBounds = true
//            return cell
//        }
        return nil
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex == 0{
           self.performSegue(withIdentifier: "SeguePopup", sender: self.chalengeModel[indexPath.row])
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ident = segue.identifier{
            if ident == "SeguePopup"{
              let dest = segue.destination as! PopupVC
                dest.eventID = self.eventId
                dest.id = (sender as! ChallengesModal).id
                dest.stitle = (sender as! ChallengesModal).title
                dest.sdescription = (sender as! ChallengesModal).description
                dest.points = "\((sender as! ChallengesModal).points!)"
                dest.delgate =  self

            }
        }
    }
    
    
    
    var eventId:String? = "4"
    var leaderBoardResponse :Json4Swift_Base?
    func fetchLeaderBords() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://yourevent2go.com/api/events/\(self.eventId!)/challenges/leaderboard?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) {[weak self] (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self?.hideHUD(forView: self?.view)
                    if let JSON = netResponse.responseDict {
                        do{
                            let jsonData = try JSONSerialization.data(withJSONObject: JSON, options:.prettyPrinted)
                            let decoder = JSONDecoder()
                            self?.leaderBoardResponse = try decoder.decode(Json4Swift_Base.self, from: jsonData)
                            self?.rowCountSectionOne = self?.leaderBoardResponse?.leader_board?.count
                            self?.rowCountSectionTwo = self?.leaderBoardResponse?.my_position?.count
                            self?.tbleView.reloadData()
                        }catch{}

                    }
                }
            }
        }
    }
    
    
  
    
    var loadNewPageData:Bool = false
    var currentPage:Int = 1;
   lazy var chalengeModel = [ChallengesModal]()
    func fetchChallanges(refreshControler:UIRefreshControl? = nil) {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://yourevent2go.com/api/events/\(self.eventId!)/challenges?user_token=\(auth_Token)&api_token=\(apiToken)&page=\(currentPage)", parameter: nil, type: .get) {[weak self] (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self?.hideHUD(forView: self?.view)
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        if (refreshControler != nil){
                            self?.chalengeModel.removeAll()
                        }
                        if JSON.count == 0{
                            self?.loadNewPageData = false
                            return
                        }
                        for jsonValue in JSON {
                            let completeModal = ChallengesModal(responseDict: jsonValue, eventId: (self?.eventId!)!)
                            self?.chalengeModel.append(completeModal)

//                            if (self?.isRefreshing)!{
//                                self?.chalengeModel.insert(completeModal, at: 0)
//                            }else{
//                                self?.chalengeModel.append(completeModal)
//                            }
                        }
                        if (self?.chalengeModel.count)! <= 0{
                            self?.view.makeToast("There is no Challenge listing".localized)
                                                }else{
                            self?.hideHUD(forView: self?.view)
                            self?.rowCountSectionOne = self?.chalengeModel.count
                            self?.cCell?.lblChanges.text = "\(self?.rowCountSectionOne!)"
                            self?.tbleView.separatorColor = UIColor.clear
                            self?.isRefreshing = false
                            self?.tbleView.reloadData()
                            self?.loadNewPageData = true
                            
                        }
                        refreshControler?.endRefreshing()
                    }
                }
                refreshControler?.endRefreshing()

            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



class TableCell:UITableViewCell{
    
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var ivUser: UIImageView!
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var vwCorner: UIView!
    @IBOutlet weak var vwTopLine: UIView!
    @IBOutlet weak var vwLeft: UIView!
    @IBOutlet weak var vwRight: UIView!
    
}

class ChallengeCell:UITableViewCell{
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwCorner: UIView!
    
}

extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
        case .bottom:
            border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
        case .right:
            border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        addSublayer(border)
    }
}


extension UIView
{
    func roundCorners(corners:UIRectCorner, radius: CGFloat)
    {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        self.layer.mask = maskLayer
    }
} 
