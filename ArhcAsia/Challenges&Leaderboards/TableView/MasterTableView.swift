//
//  MasterTableView.swift
//  CommonArchitecture
//
//  Created by Madhav on 7/21/16.
//  Copyright © 2016 Madhav. All rights reserved.
//

import UIKit

protocol MasterTableViewProtocol:class {
   func refresh(_ sender:UIRefreshControl)
   func callWebToReloadTableData()
}

extension UIViewController:MasterTableViewProtocol{
   @objc func refresh(_ sender:UIRefreshControl){
        
    }
    
    func callWebToReloadTableData(){
        
    }
}








@IBDesignable
class MasterTableView: UITableView, UIGestureRecognizerDelegate {
   internal var refreshControll: UIRefreshControl!
   weak var delegat:MasterTableViewProtocol!
    var numberOfRows:Int = 0
    
    override func draw(_ rect: CGRect) {
       // self.addObserver(self, forKeyPath: "contentOffset", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if self.visibleCells.count == 0{
            return
        }
        
        let visibleCell = self.visibleCells.last!
        let indexPath = self.indexPath(for: visibleCell)

        if (indexPath! as NSIndexPath).row == self.numberOfRows(inSection: 0) - 1{
            if self.visibleCells.count < self.numberOfRows(inSection: 0) {
                let controller:UIViewController = self.delegate! as!
                UIViewController
                self.delegat = controller
                self.delegat.callWebToReloadTableData()
                self.numberOfRows = self.numberOfRows(inSection: 0)
            }
        }
        
    }
    
    
    func refreshTable(newDataCount:NSInteger){
        var newRowCount = newDataCount - self.numberOfRows(inSection: 0)
        var baseCont =  self.numberOfRows(inSection: 0)
        var cnt  = 0
        self.beginUpdates()
        insertRows(baseCont: &baseCont, cnt: &cnt, newRowCount: &newRowCount)
    }
    
    func insertRows(baseCont: inout NSInteger, cnt:inout NSInteger,newRowCount: inout NSInteger ){
        self.insertRows(at:[IndexPath(row: baseCont + cnt, section: 0)], with:UITableViewRowAnimation.automatic)
        cnt = cnt + 1
        if cnt <= newRowCount - 1{
            insertRows(baseCont: &baseCont, cnt: &cnt, newRowCount: &newRowCount)
            return
        }
        self.endUpdates()
    }

    
    @IBInspectable var addRefreshControll:Bool = false{
        didSet{
            if addRefreshControll == true{
                refreshControll = UIRefreshControl()
                refreshControll.attributedTitle = NSAttributedString(string: "Pull to refresh")
                refreshControll.addTarget(self, action: #selector(MasterTableView.refresh(_:)), for: UIControlEvents.valueChanged)
                self.addSubview(refreshControll)
               
                
            }else{
                if refreshControll != nil{
                    refreshControll.removeFromSuperview()
                    refreshControll = nil
                }
            }
        }
    }
   
    
    @objc func refresh(_ sender:UIRefreshControl) {
        if self.delegate != nil{
            let controller:UIViewController = self.delegate! as!
            UIViewController
            self.delegat = controller
            self.delegat.refresh(sender)
        }
    }
        
    
}
