//
//  PopupVC.swift
//  ArhcAsia
//
//  Created by Namespace  on 06/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

class PopupVC: BaseClassVC {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var vwPopup: UIView!
    
    var eventID:String?
    var id:Int?
    var stitle:String?
    var points:String?
    var sdescription:String?
    weak var delgate:PopupDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        btnSubmit.layer.cornerRadius = 15
        btnSubmit.layer.borderWidth = 1
        btnSubmit.clipsToBounds = true
        txtInput.layer.cornerRadius = 15
        txtInput.layer.borderWidth = 1
        txtInput.clipsToBounds = true
        self.vwPopup.layer.cornerRadius = 15
        self.vwPopup.layer.borderWidth = 2
        self.vwPopup.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.vwPopup.clipsToBounds = true
        
        lblPoints.text = points! + " Points"
        lblDescription.text = sdescription!
        lblTitle.text = stitle!
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnOnTapSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtInput.text != nil || txtInput.text != ""{
            self.postAnswer()
        }else{
             self.view.makeToast("Please Provide a valid input".localized)
        }
    }
    
    @IBAction func btnOnTapClose(_ sender: UIButton) {
        self.dismiss(animated: false) {
            
        }
    }
    
    func postAnswer() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        let dict:[String:Any] = ["user_token":auth_Token!, "api_token":apiToken, "challenge_id":self.id!,"answer":txtInput.text!]
        if let _ = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://yourevent2go.com/api/events/\(self.eventID!)/challenges", parameter: dict, type: .post) { (netResponse) -> (Void) in
                self.hideHUD(forView: self.view)
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self.dismiss(animated: false, completion: { [weak self] in
                        self?.delgate?.showMessage()
                    })

//                    if let JSON = netResponse.responseDict as? [NSDictionary] {
//
//                    }
                }else{
                    if let msg = netResponse.responseDict?["message"] as? String{
                    self.view.makeToast(msg)
                    }else{
                        self.view.makeToast("Some error occured while submitting your answer.")
                    }
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
