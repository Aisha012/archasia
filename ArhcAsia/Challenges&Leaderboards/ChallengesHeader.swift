//
//  ChallengesHeader.swift
//  ArhcAsia
//
//  Created by Namespace  on 05/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit
class ChallengesHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var lblChanges: UILabel!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
}
