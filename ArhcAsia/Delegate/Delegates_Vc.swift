//
//  Delegates_Vc.swift
//  ToolApp
//
//  Created by Prashant Tiwari on 30/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import SafariServices
import JSSAlertView

class Delegates_Vc: BaseClassVC {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    var delegateArray = [NSDictionary]()
    var eventId: String?
    var eventData: EventData!
    var showMenuButton = true
    var sponsorPagination = SponsorPagination()
    private let refreshControl = UIRefreshControl()
    var isFirstTime = true
    var isFetchingData = false
    var categories = [Category]()
    var categoryId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        
        self.titleLabel.text = "Sponsors".localized

//        self.titleLabel.text = "Partners".localized
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        tableView.tableFooterView = UIView()

        fetchSponsors()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func refreshData(_ sender: Any) {
        self.sponsorPagination.paginationType = .reload
        self.sponsorPagination.currentPageNumber = 1
        fetchSponsors()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    func pushToWebViewWithURL(_ url: String) {
        if let url = URL(string: url) {
            if #available(iOS 11.0, *) {
                let config: SFSafariViewController.Configuration = SFSafariViewController.Configuration.init()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true, completion: nil)
            } else {
                let safariController = SFSafariViewController(url: url)
                self.present(safariController, animated: true, completion: nil)
            }
        }
    }

    func showViewsAccordingly() {
        if sponsorPagination.sponsors.count == 0 {
            tableView.isHidden = true
            emptyPlaceholderLabel.isHidden = false
        } else {
            tableView.isHidden = false
            emptyPlaceholderLabel.isHidden = true
        }
        tableView.reloadData()
    }

    func fetchSponsors(){
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            if sponsorPagination.paginationType != .old {
                self.sponsorPagination.currentPageNumber = 1
                if isFirstTime == true {
                    self.showHUD(forView: self.view, excludeViews: [])
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            isFetchingData = true
            var url = "http://yourevent2go.com/api/events/\(self.eventId!)/sponsors?user_token=\(auth_Token)&api_token=\(apiToken)&page=\(sponsorPagination.currentPageNumber)"
            if let categoryId = categoryId {
                url += "&category=\(categoryId)"
            }
            
            _ = CallApi.getData(url: url, parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self.isFetchingData = false
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        let newPagination = SponsorPagination(array: JSON)
                        self.sponsorPagination.appendDataFromObject(newPagination)
                        DispatchQueue.main.async {
                            if self.sponsorPagination.paginationType != .old && self.isFirstTime == true {
                                self.hideHUD(forView: self.view)
                                self.isFirstTime = false
                                self.showViewsAccordingly()
                            } else {
                                self.showViewsAccordingly()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                } else {
                        self.hideHUD(forView: self.view)
                }
            }
        }
        }
    }
    
    func fetchCategories() {
        
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            _ = self.showHUD(forView: self.view, excludeViews: [])
            _ = CallApi.getData(url: "http://yourevent2go.com/api/events/\(self.eventId!)/categories?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    self.hideHUD(forView: self.view)
                    if let JSON = netResponse.responseDict as? [NSDictionary] {
                        for categoryDict in JSON {
                            let category = Category(responseDict: categoryDict)
                            self.categories.append(category)
                        }
                        DispatchQueue.main.async {
                            self.categoriesButtonTapped(UIButton())
                        }
                        
                    }
                }
            }
        }
    }

    
    @IBAction func categoriesButtonTapped(_ sender: UIButton) {
        if categories.count > 0 {
            let alert = UIAlertController(title: "Filter By Category".localized, message: "", preferredStyle: .actionSheet)
            
            for category in categories {
                alert.addAction(UIAlertAction(title: category.name, style: .default) { action in
                    DispatchQueue.main.async {
                        self.categoryLabel.text = category.name
                        self.sponsorPagination.paginationType = .reload
                        self.sponsorPagination.currentPageNumber = 1
                        self.categoryId = String(category.id)
                        self.isFirstTime = true
                        self.fetchSponsors()
                    }
                })
            }
            alert.addAction(UIAlertAction(title: "All".localized, style: .default) { action in
                DispatchQueue.main.async {
                    self.categoryLabel.text = "All".localized
                    self.sponsorPagination.paginationType = .reload
                    self.sponsorPagination.currentPageNumber = 1
                    self.categoryId = nil
                    self.isFirstTime = true
                    self.fetchSponsors()

                }
            })

            alert.addAction(UIAlertAction(title: "Cancel".localized, style: .destructive) { action in
                
            })
            self.present(alert, animated: true)

        } else {
            fetchCategories()
        }
    }
    
    
}

extension Delegates_Vc : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard sponsorPagination.sponsors.count > 0  else {
            return 0
        }
        return sponsorPagination.sponsors.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < sponsorPagination.sponsors.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: SponsorTableViewCell.cellIdentifier(), for: indexPath) as! SponsorTableViewCell
        cell.configureCell(sponsor: sponsorPagination.sponsors[indexPath.row ])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < sponsorPagination.sponsors.count else {
            return sponsorPagination.hasMoreToLoad ? 50.0 : 0.0
        }
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let value = self.sponsorPagination.sponsors[indexPath.row]
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "SponsorDetailsViewController") as! SponsorDetailsViewController
        destination.sponsor = value
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.sponsorPagination.sponsors.count && self.sponsorPagination.hasMoreToLoad && !isFetchingData {
            self.sponsorPagination.paginationType = .old
            fetchSponsors()
        }
    }
    
}
