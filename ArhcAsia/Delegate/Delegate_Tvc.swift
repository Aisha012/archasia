//
//  Delegate_Tvc.swift
//  ToolApp
//
//  Created by Prashant Tiwari on 30/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit

class Delegate_Tvc: UITableViewCell {

    @IBOutlet weak var delegate_img: UIImageView!
    @IBOutlet weak var delegate_name: UILabel!
    @IBOutlet weak var delegate_Category: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
