//
//  ConversationTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class ConversationTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var previousMessageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var roleLabel: UILabel!
    
    
    class func cellIdentifier() -> String {
        return "ConversationTableViewCell"
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 4
        self.holderView.layer.cornerRadius = 5
        self.holderView.layer.shadowOpacity = 0.3
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false
    }
    
    func configureCell(convo: Conversation) {
        let avatarURL = convo.avatar ?? ""
        profileImageView.sd_setShowActivityIndicatorView(true)
        profileImageView.sd_setIndicatorStyle(.gray)
        profileImageView.sd_setImage(with: URL(string: avatarURL), placeholderImage: UIImage(named: "profile.png"), options: .progressiveDownload, completed: nil)
        
        nameLabel.text = convo.firstName + " " + convo.lastName
        previousMessageLabel.text = convo.lastMessage
        timeLabel.text = convo.elapsedTime
        if convo.position.isEmpty && convo.organization.isEmpty {
            roleLabel.text = ""
        } else if convo.position.isEmpty {
            roleLabel.text = convo.organization
        } else if convo.organization.isEmpty {
            roleLabel.text = convo.position
        } else {
            roleLabel.text = convo.position + " at " + convo.organization
        }
    }
}
