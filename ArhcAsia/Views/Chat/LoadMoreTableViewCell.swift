//
//  LoadMoreTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class LoadMoreTableViewCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    class func cellIdentifier() -> String {
        return "LoadMoreTableViewCell"
    }

}
