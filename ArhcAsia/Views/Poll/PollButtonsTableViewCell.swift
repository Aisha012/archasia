//
//  PollButtonsTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 17/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

protocol PollButtonsTableViewCellDelegate {
    func submitButtonTapped()
    func previousButtonTapped()
    func nextButtonTapped()
}

class PollButtonsTableViewCell: UITableViewCell {

    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var nextQuestionButton: UIButton!
    @IBOutlet weak var previousQuestionButton: UIButton!
    
    var delegate: PollButtonsTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "PollButtonsTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.submitButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.submitButton.layer.shadowColor = UIColor.black.cgColor
        self.submitButton.layer.shadowRadius = 4
        self.submitButton.layer.cornerRadius = 5
        self.submitButton.layer.borderColor = UIColor.black.cgColor
        self.submitButton.layer.borderWidth = 2
        self.submitButton.layer.shadowOpacity = 0.3
        self.submitButton.layer.masksToBounds = false
        self.submitButton.layer.rasterizationScale = UIScreen.main.scale
        self.submitButton.clipsToBounds = false
    }

    @IBAction func previousButtonTapped(_ sender: UIButton) {
        delegate?.previousButtonTapped()
    }
 
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        delegate?.nextButtonTapped()
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        delegate?.submitButtonTapped()
    }
    
}
