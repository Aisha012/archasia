//
//  AnswerTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 18/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class AnswerTableViewCell: UITableViewCell {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var roleLabel: UILabel!

    class func cellIdentifier() -> String {
        return "AnswerTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 4
        self.holderView.layer.cornerRadius = 5
        self.holderView.layer.shadowOpacity = 0.3
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false
        
    }
    
    func configureCell(answer: Answer) {
        let avatarURL = answer.avatar ?? ""
        profilePic.sd_setShowActivityIndicatorView(true)
        profilePic.sd_setIndicatorStyle(.gray)
        profilePic.sd_setImage(with: URL(string: avatarURL), placeholderImage: UIImage(named: "profile.png"), options: .progressiveDownload, completed: nil)
        
        nameLabel.text = answer.firstName + " " + answer.lastName
        dateLabel.text = answer.formattedDate
        answerLabel.text = answer.answer
        roleLabel.text = answer.role
    }

    
}
