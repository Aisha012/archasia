//
//  AgendaDescriptionTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 14/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class AgendaDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return  "AgendaDescriptionTableViewCell"
    }
    
}
