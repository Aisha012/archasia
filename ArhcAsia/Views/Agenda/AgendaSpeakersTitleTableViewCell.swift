//
//  AgendaSpeakersTitleTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 22/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class AgendaSpeakersTitleTableViewCell: UITableViewCell {

    class func cellIdentifier() -> String {
        return "AgendaSpeakersTitleTableViewCell"
    }

}
