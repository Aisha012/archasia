//
//  BreakOutTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 26/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class BreakOutTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "BreakOutTableViewCell"
    }

    func configureCell(_ breakoutSession: BreakOutSession) {
        titleLabel.text = breakoutSession.name
        let date = AgendaListingVc.convertDate(getDate: breakoutSession.startTime) + " - " + AgendaListingVc.convertDate(getDate: breakoutSession.endTime)
        timeLabel.text = date
        descLabel.text = breakoutSession.desc
    }
}
