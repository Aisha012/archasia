//
//  SponsorTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 19/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class SponsorTableViewCell: UITableViewCell {

    @IBOutlet weak var sponsorImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "SponsorTableViewCell"
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 4
        self.holderView.layer.cornerRadius = 5
        self.holderView.layer.shadowOpacity = 0.3
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false
    }
    
    func configureCell(sponsor: Sponsor) {
        let avatarURL = sponsor.logo ?? ""
        sponsorImageView.sd_setShowActivityIndicatorView(true)
        sponsorImageView.sd_setIndicatorStyle(.gray)
        sponsorImageView.sd_setImage(with: URL(string: avatarURL), placeholderImage: nil, options: .progressiveDownload, completed: nil)
        
        nameLabel.text = sponsor.name
        if let category = sponsor.category, !category.isEmpty {
            categoryLabel.isHidden = false
            categoryLabel.text = category
        } else {
            categoryLabel.isHidden = true
        }
    }

    
}
