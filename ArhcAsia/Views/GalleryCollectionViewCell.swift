//
//  GalleryCollectionViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 26/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var galleryImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "GalleryCollectionViewCell"
    }
    
}
