//
//  BannerData+CoreDataProperties.swift
//  ToolApp
//
//  Created by Zaman Meraj on 03/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//
//

import Foundation
import CoreData


extension BannerData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BannerData> {
        return NSFetchRequest<BannerData>(entityName: "BannerData")
    }

    @NSManaged public var banner_id: String?
    @NSManaged public var banner_title: String?
    @NSManaged public var banner_description: String?
    @NSManaged public var banner_image: String?
    @NSManaged public var event_id: String?
    @NSManaged public var banner_video: String?

}
