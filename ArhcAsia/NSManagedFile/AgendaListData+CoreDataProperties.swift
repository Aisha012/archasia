//
//  AgendaListData+CoreDataProperties.swift
//  ToolApp
//
//  Created by Zaman Meraj on 06/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//
//

import Foundation
import CoreData


extension AgendaListData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AgendaListData> {
        return NSFetchRequest<AgendaListData>(entityName: "AgendaListData")
    }

    @NSManaged public var description_text: String?
    @NSManaged public var end_date: String?
    @NSManaged public var id: String?
    @NSManaged public var start_date: String?
    @NSManaged public var time_range: String?
    @NSManaged public var title: String?
    @NSManaged public var event_id: String?

}
