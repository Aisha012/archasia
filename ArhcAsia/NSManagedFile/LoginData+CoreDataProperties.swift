//
//  LoginData+CoreDataProperties.swift
//  ToolApp
//
//  Created by Zaman Meraj on 03/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//
//

import Foundation
import CoreData


extension LoginData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LoginData> {
        return NSFetchRequest<LoginData>(entityName: "LoginData")
    }

    @NSManaged public var authenticate_user: String?
    @NSManaged public var avatar: String?
    @NSManaged public var email: String?
    @NSManaged public var first_name: String?
    @NSManaged public var last_name: String?
    @NSManaged public var mobile: String?
    @NSManaged public var qrcode: String?
    @NSManaged public var role: String?
    @NSManaged public var user_id: String?
    @NSManaged public var position: String?
    @NSManaged public var company: String?

}
