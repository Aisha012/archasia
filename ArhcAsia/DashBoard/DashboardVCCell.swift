//
//  DashboardVCCell.swift
//  ToolApp
//
//  Created by Zaman Meraj on 26/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit

class DashboardVCCell: UICollectionViewCell {

    @IBOutlet weak var tileText: UILabel!
    @IBOutlet weak var tileImage: UIImageView!
    @IBOutlet weak var tileView: UIView!
    @IBOutlet weak var myContentView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.tileView.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.tileView.layer.shadowColor = UIColor.lightGray.cgColor
        self.tileView.layer.shadowRadius = 4
//        self.shadowView.layer.cornerRadius = 5
        self.tileView.layer.shadowOpacity = 0.3
        self.tileView.layer.masksToBounds = false
        self.tileView.layer.rasterizationScale = UIScreen.main.scale
        self.tileView.clipsToBounds = false
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        self.tileView.layer.borderColor = UIColor.white.cgColor
//        self.tileView.layer.borderWidth = 1
    }

}
