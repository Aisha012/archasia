//
//  DashboardVC.swift
//  ToolApp
//
//  Created by Zaman Meraj on 26/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import SDWebImage
import ASPVideoPlayer

class DashboardVC: BaseClassVC, SWRevealViewControllerDelegate {

    @IBOutlet weak var menuCollection: UICollectionView!
    @IBOutlet weak var myCOntentView: UIView!
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    
    var offSet: CGFloat = 0
    let cellId = "DashboardVCCell"
    let headerId = "DashboardVCHeader"
    var tilesArray = ["agenda", "venue", "Event", "sponsors", "delegates", "speaker", "gallery", "CheckIn"]
    //            aish
    //            change access to venue change
//    var tilesArray = ["agenda", "access", "Event", "sponsors", "delegates", "speaker", "gallery", "CheckIn"]
    var eventData: EventData?
    var showTiles = [String]()
    var tileImage = [String]()
    var headerImage = [String]()
    var videoURL = ""
    var eventId: String?
    let videoPlayer = ASPVideoPlayerView()
    var currentUser: LoginData!
    var pages: [PageData]?

    override func viewDidLoad() {
        super.viewDidLoad()
        Utilities.shared.registerDeviceToken()
        let revealViewController = self.revealViewController()
        menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
//        aish change Event starts in to notification
        self.titleLabel.text = "Notifications".localized

//        self.titleLabel.text = "Event starts in".localized
        let userID = UserDefaults.standard.value(forKey: "UUID") as! String
        let loginData = MyDatabase.matchProgramData(tableName: "LoginData", tableId: userID
            , columnName: "user_id") as? [LoginData]
        currentUser = loginData![0]

        UserDefaults.standard.set(self.eventId!, forKey: "EventId")
        self.setCollectionViewDatasource()
        self.fetchData()
        self.setUpApp()
        self.setScrollView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setUpTimerLabel() {
        let timer = Timer.scheduledTimer(timeInterval: 1,
                             target: self,
                             selector: #selector(self.updateTime),
                             userInfo: nil,
                             repeats: true)
        RunLoop.current.add(timer, forMode: .commonModes)
    }
    
    @objc func updateTime() {
        self.titleLabel.text = getElapsedTimefromDate(startTime: eventData?.start_time)
    }
    
    func setCollectionViewDatasource() {
        self.menuCollection.dataSource = self
        self.menuCollection.delegate    = self
        self.menuCollection.register(UINib.init(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
        self.menuCollection.register(UINib.init(nibName: cellId, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        if let layout = self.menuCollection.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumInteritemSpacing = 1
            layout.minimumLineSpacing      = 1
        }
    }
    
    func fetchData() {
        let eventsData = MyDatabase.matchProgramData(tableName: "EventData", tableId: self.eventId!, columnName: "event_id") as? [EventData]
        pages = MyDatabase.matchProgramData(tableName: "PageData", tableId: self.eventId!, columnName: "event_id") as? [PageData]
        guard let banners = MyDatabase.matchProgramData(tableName: "BannerData", tableId: self.eventId!, columnName: "event_id") as? [BannerData] else { return }
        UserDefaults.standard.set(self.eventId!, forKey: "Event_Id")
        if banners.count > 0 {
            for (index, banner) in banners.enumerated() {
                let bannerImageURL = banner.banner_image!
                let bannerVideoURL = banner.banner_video!
                if index == 0, !bannerVideoURL.isEmpty {
                    self.videoURL = bannerVideoURL
                    break
                } else if bannerVideoURL.isEmpty {
                    self.headerImage.append(bannerImageURL)
                }
            }
        }
        
        if let eventsData = eventsData {
            let eventData = eventsData[0]
            self.eventData = eventData
//            aish
//            self.setUpTimerLabel()
            if eventData.agendas == "1" {
                self.showTiles.append("Agenda".localized)
                self.tileImage.append("tile_agenda")
            }
//            aish
//            change access to venue
            self.showTiles.append("Venue".localized)
            self.tileImage.append("tile_access")
            if eventData.is_floor_plan == "1" {
                self.showTiles.append("Floor Plan".localized)
                self.tileImage.append("tile_eventfloorplan")
            }
            if eventData.sponsors ==  "1"{
//                change partner to sponsers aish
                self.showTiles.append("Sponsors".localized)

//                self.showTiles.append("Partners".localized)
                self.tileImage.append("tile_partners")
            }
            if eventData.speakers == "1" {
                self.showTiles.append("Speakers".localized)
                self.tileImage.append("tile_speakers")
            }
            if eventData.gallery == "1" {
                self.showTiles.append("Gallery".localized)
                self.tileImage.append("tile_gallery")
            }
            if let role = currentUser.role, role != "normal" {
                self.showTiles.append("Check In".localized)
                self.tileImage.append("tile_checkIn")
            }
            if eventData.document == "1" {
                self.showTiles.append("Documents".localized)
                self.tileImage.append("tile_documents")
            }
            if eventData.chat == "1" {
                self.showTiles.append("Delegates".localized)
                self.tileImage.append("tile_delegates")
            }
            
            if eventData.challenge == "1" {
                self.showTiles.append("Challenge".localized)
                self.tileImage.append("tile_challange")
            }
            
            if let pagesData = pages, pagesData.count > 0 {
                self.showTiles.append("More".localized)
                self.tileImage.append("tile_info")
                
            }
            self.showTiles.append("Feedback".localized)
            self.tileImage.append("FeedBack")
            self.menuCollection.reloadData()
        }
    }
    
    func setUpApp() {
        let colorValue: UIColor!
        if let appColor = self.eventData?.app_color {
            colorValue = UIColor.colorWithHexString(hex: appColor)
            UserDefaults.standard.set(appColor, forKey: "color_Value")
        }else{
            colorValue = UIColor.colorWithHexString(hex: "#00C3E7")
            UserDefaults.standard.set("#00C3E7", forKey: "color_Value")
        }
//        self.view.backgroundColor = colorValue
        customNavBar.backgroundColor = navigationBarColor()
//        bottomView.backgroundColor = navigationBarColor()
    }
    
//    func setScrollView() {
//
//        self.offSet = 0
//        if !self.videoURL.isEmpty {
//
//            videoPlayer.videoURL = URL(string: self.videoURL)
//            videoPlayer.frame = CGRect.init(x: 0, y: imageScrollView.frame.origin.y, width: ScreenWidth, height: imageScrollView.frame.size.height)
////            imageScrollView.frame
//            videoPlayer.shouldLoop = true
//            videoPlayer.backgroundColor = UIColor.lightGray
//            imageScrollView.addSubview(videoPlayer)
//
//            videoPlayer.readyToPlayVideo = {
//                self.videoPlayer.playVideo()
//            }
//            videoPlayer.volume = 0
//            videoPlayer.startedVideo = {
//                print("Video has started playing.")
//            }
//        } else {
//            _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
//            for (index, image) in headerImage.enumerated() {
//                let image = image
//                let imageView = UIImageView()
//                imageView.contentMode = .scaleAspectFill
//                guard let url = URL(string: image) else { return }
//                imageView.sd_setImage(with: url, placeholderImage: nil, options: .progressiveDownload, progress: nil, completed: nil)
////                imageView.contentMode = .scaleAspectFit
////                imageView.frame     =   imageScrollView.frame
////                imageView.frame.width = ScreenWidth - 8
//                imageView.frame = CGRect.init(x: 0, y: imageScrollView.frame.origin.y, width: ScreenWidth, height: imageScrollView.frame.size.height)
//
//                imageView.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
//                    imageScrollView.addSubview(imageView)
//            }
//        }
//
//    }
    func setScrollView() {
        
        self.offSet = 0
        if !self.videoURL.isEmpty {
            
            videoPlayer.videoURL = URL(string: self.videoURL)
            videoPlayer.frame = CGRect.init(x: 0, y: imageScrollView.frame.origin.y, width: ScreenWidth, height: imageScrollView.frame.size.height)
            videoPlayer.shouldLoop = true
            videoPlayer.backgroundColor = UIColor.lightGray
            imageScrollView.addSubview(videoPlayer)
            
            videoPlayer.readyToPlayVideo = {
                self.videoPlayer.playVideo()
            }
            videoPlayer.volume = 0
            videoPlayer.startedVideo = {
                print("Video has started playing.")
            }
        } else {
            _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
            for (index, image) in headerImage.enumerated() {
                let image = image
                let imageView = UIImageView()
//                imageView.contentMode = 
                imageView.clipsToBounds = true
                guard let url = URL(string: image) else { return }
                imageView.sd_setImage(with: url, placeholderImage: nil, options: .progressiveDownload, progress: nil, completed: nil)
                imageView.frame = CGRect.init(x: 0, y: imageScrollView.frame.origin.y, width: ScreenWidth, height: imageScrollView.frame.size.height)
                imageView.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
                imageScrollView.addSubview(imageView)
            }
        }
        
    }
    
    @objc func autoScroll() {
        let totalPossibleOffset = CGFloat(headerImage.count - 1) * self.view.bounds.size.width
        if offSet == totalPossibleOffset {
            offSet = 0 // come back to the first image after the last image
        }
        else {
            offSet += self.view.bounds.size.width
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.imageScrollView.contentOffset.x = CGFloat(self.offSet)
            }, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension DashboardVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.showTiles.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DashboardVCCell
        
        cell.tileText.text = self.showTiles[indexPath.row]
//            .uppercased()
        cell.tileText.textColor = navigationBarColor()
        
        let imageName = self.tileImage[indexPath.row]
        let originalImage = UIImage(named: imageName)
        let templateImage = originalImage!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        cell.tileImage.image = templateImage
        cell.tileImage.tintColor = navigationBarColor()

//        cell.tileImage.image = UIImage(named: imageName)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath)

        self.imageScrollView.frame = header.frame
        self.setScrollView()
        header.addSubview(self.imageScrollView)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellLength = (self.menuCollection.frame.width/3)
        return CGSize(width: cellLength, height: cellLength)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    //    - (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//    {
//    return 10; // This is the minimum inter item spacing, can be more
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let titleText = self.showTiles[indexPath.row]
        self.btnTapped(text: titleText)
    }
    
    func btnTapped(text: String) {
        let eventsData = MyDatabase.matchProgramData(tableName: "EventData", tableId: self.eventId!, columnName: "event_id") as? [EventData]
        switch text {
            
        case "Speakers":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "Spekers_VC") as! Spekers_VC
            destination.eventData = eventsData![0]
            let eventData = eventsData![0]
            destination.showMenuButton = false
            destination.app_token  = eventData.app_token!
            self.navigationController?.pushViewController(destination, animated: true)
            break
        case "Sponsors" :
//        case "Partners":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "Delegates_Vc") as! Delegates_Vc
            destination.eventData = eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)
            break

        case "Floor Plan":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "MultiFloorPlanViewController") as! MultiFloorPlanViewController
            destination.eventData =   eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)
            break

        case "Gallery":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "PhotoGallaryVc") as! PhotoGallaryVc
            destination.eventData = eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)
            break

//        case "Access":
        case "Venue":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "AddressVC") as! AddressVC
            destination.eventData = eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)
            break

        case "Agenda":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "AgendaListViewController") as! AgendaListViewController
            destination.eventData = eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)
            break

//            let destination = self.storyboard?.instantiateViewController(withIdentifier: "AgendaListingVc") as! AgendaListingVc
//            destination.eventData = eventsData![0]
//            destination.showMenuButton = false
//            self.navigationController?.pushViewController(destination, animated: true)
            
        case "Check In":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "CheckInVc") as! CheckInVc
            destination.eventData = eventsData![0]
            destination.showMenuButton = false
            self.navigationController?.pushViewController(destination, animated: true)
            break
          
        case "Delegates":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "ConversationViewController") as! ConversationViewController
            destination.showMenuButton = false
            destination.eventData = eventsData![0]
            self.navigationController?.pushViewController(destination, animated: true)
            break
        case "More":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "PagesViewController") as! PagesViewController
            destination.showMenuButton = false
            destination.pages = pages
            self.navigationController?.pushViewController(destination, animated: true)
            break
        case "Documents":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "DocumentListViewController") as! DocumentListViewController
            destination.showMenuButton = false
            destination.eventData = eventsData![0]
            self.navigationController?.pushViewController(destination, animated: true)
            break
        case "Challenge":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "ChallengesNLeaderboardVC") as! ChallengesNLeaderboardVC
            destination.showMenuButton = false
            destination.eventData = eventsData![0]
            self.navigationController?.pushViewController(destination, animated: true)
            break
        case "Feedback":
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "FeedBackController") as! FeedBackController
            destination.showMenuButton = false
//            destination.eventData = eventsData![0]
            self.navigationController?.pushViewController(destination, animated: true)
            break
        default:
            break
            
            
        }
        
    }

    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        if position == FrontViewPosition.right {
            revealController.frontViewController.view.isUserInteractionEnabled = false
        } else {
            revealController.frontViewController.view.isUserInteractionEnabled = true
        }
    }

    @IBAction func notificationBtnTapped(_ sender : UIButton){
        self.performSegue(withIdentifier: "SegueNotification", sender: nil)
        print("tapped")
    }
}





