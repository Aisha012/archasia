//
//  UIExtensions.swift
//  CoopCommerce
//
//  Created by Beeline2 on 10/25/16.
//  Copyright © 2016 Brevity. All rights reserved.
//

import UIKit

@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}

@IBDesignable extension UITextView {
    @IBInspectable var placeHolderText:NSString? {
        set {
            self.text = newValue as! String
        }
        get {
            if let text = self.text {
                return text as NSString?
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var placeHolderColor:UIColor {
        set {
            self.textColor = newValue
        }
        get {
            if let color = self.textColor {
                return color
            } else {
                return UIColor.lightGray
            }
        }
    }
}


extension String{
    func localized()->String{
        return NSLocalizedString(self, comment: "")
    }
    
    fileprivate func retrieveLocalized(){
        let path = Bundle.main.path(forResource: "Localizable", ofType: "strings")
        let localizedDict =  NSMutableDictionary(contentsOfFile: path!)
        guard let allKeys = localizedDict?.allKeys else {
            return
        }
        
    }
}



extension UITextField{
//    func setLeftImageWithName(imageName:String){
//        let imgView = UIImageView(image: UIImage(named: imageName))
//        imgView.frame = CGRectMake(10.0, 0.0, (imgView.image?.size.width)!, (imgView.image?.size.height)!)
//        let paddingView = UIView(frame: CGRectMake(0.0, 0.0, ((imgView.image?.size.width)! + 10) + 10.0, (imgView.image?.size.height)! + 10))
//        paddingView.addSubview(imgView)
//        self.leftView = paddingView
//        self.leftViewMode = UITextFieldViewMode.Always
//    }
    
//    func setRightImageWithName(imageName:String){
//        let imgView = UIImageView(image: UIImage(named: imageName))
//        imgView.frame = CGRectMake(10.0, 8.0, (imgView.image?.size.width)! , (imgView.image?.size.height)!)
//        let paddingView = UIView(frame: CGRectMake(0.0, 0.0, ((imgView.image?.size.width)! + 10), (imgView.image?.size.height)! + 10))
//        paddingView.addSubview(imgView)
//        self.rightView = paddingView
//        self.rightViewMode = UITextFieldViewMode.Always
//    }
    
//    func setRightImageWithName(imageName:String, button:UIButton){
//        let image = UIImage(named: imageName)
//        button.frame = CGRectMake(10.0, 8.0, (image!.size.width) , (image!.size.height))
//        let paddingView = UIView(frame: CGRectMake(0.0, 0.0, ((image!.size.width) + 10), (image!.size.height) + 10))
//        paddingView.addSubview(button)
//        self.rightView = paddingView
//        self.rightViewMode = UITextFieldViewMode.Always
//    }
    
    
    func isTextFieldEmpty() -> Bool {
        return ((self.text?.trimmingCharacters(in: .whitespacesAndNewlines)) == nil)
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!)
    }
    
    func isValidTextLength(minValue: Int, maxValue: Int) -> Bool {
        guard  let strText = (self.text?.trimmingCharacters(in: .whitespacesAndNewlines)) else{
            return false
        }
        if ((strText.characters.count) < minValue) || ((strText.characters.count) > maxValue) {
            return false
        }
        return true
    }
    
    
    
}

